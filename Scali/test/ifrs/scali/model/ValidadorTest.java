/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.model;

import java.security.MessageDigest;
import java.sql.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aluno
 */
public class ValidadorTest {
    
    public ValidadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /* Testes de CPF. */
    
    @Test 
    public void testBlankCpf(){
        String cpf = "";
        boolean expResult = false;
        boolean result = Util.isCpfValid(cpf);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNullCpf(){
        String cpf = null;
        boolean expResult = false;
        boolean result = Util.isCpfValid(cpf);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testInvalidCpf(){
        String cpf[] = {
            "111.111.111-11",
            "222.222.222-22",
            "333.333.333-33",
            "444.444.444-44",
            "555.555.555-55",
            "673.411.720-03"
        };
        for(int i = 0; i < cpf.length; i++){
            assertEquals(false, Util.isCpfValid(cpf[i]));
        }
    }
    
    
    @Test 
    public void testValidCpf(){
        String cpf[] = {
            "662.265.140-00",
            "502.443.780-29",
            "085.090.260-68",
            "086.410.060-44",
            "204.791.200-80",
            "673.411.720-02"
        };
        
        for(int i = 0; i < cpf.length; i++){
            boolean b = Util.isCpfValid(cpf[i]); 
            assertEquals(true, b);
        }
    }
    
    /* Testes de CNPJ */
    
    @Test 
    public void testBlankCnpj(){
        String cpf = "";
        boolean expResult = false;
        boolean result = Util.isCnpjValid(cpf);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNullCnpj(){
        String cpf = null;
        boolean expResult = false;
        boolean result = Util.isCnpjValid(cpf);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testInalidCnpj(){
        String cnpj[] = {
            "75.235.737/0001-93",
            "38.525.283/0001-62",
            "41.210.603/0001-05",
            "50.990.591/0001-61",
            "37.787.954/0001-09",
            "74.504.023/0001-71"
        };
        for (String cnpj1 : cnpj) {
            assertEquals(false, Util.isCnpjValid(cnpj1));
        }
    }
            
            
    @Test
    public void testValidCnpj(){
        String cnpj[] = {
            "11.222.333/0001-81",
            "75.235.737/0001-92",
            "38.525.283/0001-68",
            "41.210.603/0001-03",
            "50.990.591/0001-60",
            "37.787.954/0001-04",
            "74.504.023/0001-70"
        };
        for (String cnpj1 : cnpj) {
            boolean result = Util.isCnpjValid(cnpj1);
            assertEquals(true, result);
            
        }
    }
    
    
    @Test
    public void testCpfConvert(){
        String cpf[] = {
            "662.265.140-00",
            "502.443.780-29",
            "085.090.260-68",
            "086.410.060-44",
            "204.791.200-80",
            "673.411.720-02"
        };
        
        for(int i = 0; i < cpf.length; i++){
            String b = Util.cpfFromLong(Util.cpfToLong(cpf[i]));
            assertEquals(cpf[i], b);
        }
    }
    
    
    @Test
    public void testCnpjConvert(){
        String cnpj[] = {
            "11.222.333/0001-81",
            "75.235.737/0001-92",
            "38.525.283/0001-68",
            "41.210.603/0001-03",
            "50.990.591/0001-60",
            "37.787.954/0001-04",
            "74.504.023/0001-70"
        };
        
        
        for(int i = 0; i < cnpj.length; i++){
            long l = Util.cnpjToLong(cnpj[i]);
            String b = Util.cnpjFromLong(l);
            assertEquals(cnpj[i], b);
        }
    }
    
    @Test
    public void testBuscaCepOnline(){
        //Endereco end = Util.buscarPorCepOnline("94965-020");
    }
    
    @Test
    public void testCepToLong(){
        String[] ceps = {
            "78990-000",
            "94965-020",
            "92310-300"
        };
        long[] numeric = {
            78990000L,
            94965020L,
            92310300L
        };
        for(int i = 0; i < ceps.length; i++){
            assertEquals(numeric[i], Util.cepToLong(ceps[i]));
        }
    }
    
    @Test
    public void testValidCep(){
        String[] ceps = {
            "78990-000",
            "94965-020",
            "92310-300"
        };
        for(int i = 0; i < ceps.length; i++){
            assertEquals(true, Util.isCepValid(ceps[i]));
        }
    }
    @Test
    public void testInvalidCep(){
        String[] ceps = {
            "78990-0x0",
            "949655020",
            "9230-300"
        };
        for(int i = 0; i < ceps.length; i++){
            assertEquals(false, Util.isCepValid(ceps[i]));
        }
    }
    
    @Test
    public void testCepFromLong(){
        String[] ceps1 = {
            "78990-000",
            "94965-020",
            "92310-300"
        };
        long ceps2[] = {
            78990000,
            94965020,
            92310300
        };
        for(int i = 0; i < ceps1.length; i++){
            assertEquals(ceps1[i], Util.cepFromLong(ceps2[i]));
        }
    }
    
    @Test
    public void testCategorias(){
        String[] testes = {
            "AXXXX",
            "XXXXX",
            "XBXXX",
            "AXXDX",
            "XXXDE",
            "ABXDX"
        };
        boolean[][] teste2 = {
            {true,  false, false, false, false},
            {false, false, false, false, false},
            {false, true,  false, false, false},
            {true,  false, false, true,  false},
            {false, false, false, true,  true},
            {true,  true,  false, true,  false},
        };
        
        for(int i = 0; i < testes.length; i ++){
            boolean[] result = Util.categoriasFromString(testes[i]);
            for(int j = 0; j < 5; j++){
                assertEquals(result[j], teste2[i][j]);
            }
            
        }
    }
    
    public void testCategoriasToString(){
        String[] testes = {
            "AXXXX",
            "XXXXX",
            "XBXXX",
            "AXXDX",
            "XXXDE"
        };
        boolean[][] teste2 = {
            {true,  false, false, false, false},
            {false, false, false, false, false},
            {false, true,  false, false, false},
            {true,  false, false, true,  false},
            {false, false, false, true,  true},
        };
    
        for(int i = 0; i < testes.length; i ++){
            assertEquals((testes[i]), Util.categoriasToString(teste2[i]));
            assertEquals((teste2[i]), Util.categoriasFromString(testes[i]));
        }
    }
    
   @Test
   public void testNomeCurtoDemais(){
       String[] testes = {
           "an",
           "te",
           "J     m",
           "tl101",
           ""
       };
       for(int i = 0; i < testes.length; i ++){
            assertEquals(false, Util.isNameValid(testes[i]));
        }
   }
   
   @Test
   public void testNomeLongo(){
       String[] testes = {
           "ana laura",
           "teste",
           "João Armam",
           "cliente101",
           "Cliente1"
       };
       for(int i = 0; i < testes.length; i ++){
            assertEquals(true, Util.isNameValid(testes[i]));
        }
   }
}
