/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.database;

import ifrs.scali.model.Endereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Léo H.
 */
public class EnderecoDB {
  
    public static Endereco getOne(int id){
        Endereco e = new Endereco();
        String sql = "SELECT * FROM MR_ENDERECO WHERE ID=?";
        Connection conn = new Conexao().getConexao();
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet res = st.executeQuery();
            if(res.next()){
                e.setBairro(res.getString("bairro"));
                e.setCep(res.getString("cep"));
                e.setCidade(res.getString("cidade"));
                e.setComplemento(res.getString("complemento"));
                e.setEstado(res.getString("estado"));
                e.setId(id);
                e.setLogradouro(res.getString("logradouro"));
                e.setNumero(res.getInt("numero"));
                return e;
            }
        } catch(SQLException ex){
            System.out.println("Exception Endereco.getOne()");
            System.out.println(ex);
        }
        return null;
    }
    public static boolean insert(Endereco end){
        Connection conn = new Conexao().getConexao();
        String sql = "INSERT INTO MR_ENDERECO VALUES (enderecos.nextval, ?, ?, ?, ?, ?, ?, ?)";
        String[] generatedColumns = {"id"};
        try{
            PreparedStatement ps = conn.prepareStatement(sql, generatedColumns);
            ps.setString(1, end.getCep());
            ps.setString(2, end.getLogradouro());
            ps.setInt(3, end.getNumero());
            ps.setString(4, end.getCidade());
            ps.setString(5, end.getEstado());
            ps.setString(6, end.getComplemento());
            ps.setString(7, end.getBairro());
            ps.execute();
            
            ResultSet rs = ps.getGeneratedKeys();
            
            if (rs.next()) {
                end.setId(rs.getInt(1));
            } else {
                System.out.println("insertEndereço não retornou id.");
                return false;
            }
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
        return true;
    }
    
    public static boolean update(Endereco end){
        Connection conn = new Conexao().getConexao();
        String sql = "UPDATE MR_ENDERECO SET CEP = ?, LOGRADOURO = ?, NUMERO = ?, CIDADE = ?, ESTADO = ?, COMPLEMENTO = ?, BAIRRO = ? WHERE ID=?";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, end.getCep());
            ps.setString(2, end.getLogradouro());
            ps.setInt(3, end.getNumero());
            ps.setString(4, end.getCidade());
            ps.setString(5, end.getEstado());
            ps.setString(6, end.getComplemento());
            ps.setString(7, end.getBairro());
            ps.setInt(8, end.getId());
            ps.execute();
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
        return true;
    }
}
