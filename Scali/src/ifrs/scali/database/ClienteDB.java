/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import ifrs.scali.model.Cliente;
import ifrs.scali.model.Cliente.TipoCliente;
import static ifrs.scali.model.Cliente.TipoCliente.PESSOA_FISICA;
import static ifrs.scali.model.Cliente.TipoCliente.PESSOA_JURIDICA;
import ifrs.scali.model.Util;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Types;
/**
 *
 * @author Léo H.
 */
public class ClienteDB {
    public static ArrayList<Cliente> getAll(){
        String selectSQL = "SELECT * FROM mr_cliente WHERE deletado = 0";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement s = null;
        ArrayList <Cliente> clientes = new ArrayList<Cliente>();
        try{
            s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Cliente cl = new Cliente();
                cl.setId(rs.getInt("ID"));
                cl.setCpf(rs.getLong("CPF"));
                cl.setCnpj(rs.getLong("CNPJ"));
                cl.setName(rs.getString("NOME"));
                cl.setTipo(rs.getInt("TIPO")==1 ? PESSOA_FISICA : PESSOA_JURIDICA);
                cl.setTelefone(rs.getString("TELEFONE")); 
                cl.setCadastro(rs.getDate("DATA_CADASTRO"));
                cl.setEndereco(EnderecoDB.getOne(rs.getInt("ENDERECO")));                         
                clientes.add(cl);    
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return clientes;
    }
    public static ArrayList<Cliente> getAllWhereClause(String clause){
        String selectSQL = "SELECT * FROM mr_cliente where deletado = 0";
        clause = " WHERE " + clause;
        selectSQL = selectSQL + clause; 
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement s = null;
        ArrayList <Cliente> clientes = new ArrayList<Cliente>();
        try{
            s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Cliente cl = new Cliente();
                cl.setId(rs.getInt("ID"));
                cl.setCpf(rs.getLong("CPF"));
                cl.setCnpj(rs.getLong("CNPJ"));
                cl.setName(rs.getString("NOME"));
                cl.setTipo(rs.getInt("TIPO")==1 ? PESSOA_FISICA : PESSOA_JURIDICA);
                cl.setTelefone(rs.getString("TELEFONE")); 
                cl.setCadastro(rs.getDate("DATA_CADASTRO"));
                cl.setEndereco(EnderecoDB.getOne(rs.getInt("ENDERECO")));                         
                clientes.add(cl);    
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return clientes;
    }
    
    public static ArrayList<Cliente> getAllLike(String text){
        String selectSQL = "SELECT * FROM mr_cliente WHERE CPF like ('%' + ? + '%') OR nome like ('%' + ? + '%') or endereco like ('%' + ? + '%') ";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList <Cliente> clientes = new ArrayList<Cliente>();
        try{
            PreparedStatement s = dbConnection.prepareStatement(selectSQL);
            s.setString(1, text);
            s.setString(2, text);
            s.setString(3, text);
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Cliente cl = new Cliente();
                cl.setId(rs.getInt("ID"));
                cl.setCpf(rs.getLong("CPF"));
                cl.setCnpj(rs.getLong("CNPJ"));
                cl.setName(rs.getString("NOME"));
                cl.setTipo(rs.getInt("TIPO")==1 ? PESSOA_FISICA : PESSOA_JURIDICA);
                cl.setTelefone(rs.getString("TELEFONE")); 
                cl.setCadastro(rs.getDate("DATA_CADASTRO"));  
                cl.setEndereco(EnderecoDB.getOne(rs.getInt("ENDERECO")));                       
                clientes.add(cl);    
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return clientes;
    }
    
    
    public static Cliente getOne(int id){
        String selectSQL = "SELECT * FROM mr_cliente where id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;    
        Cliente cl = new Cliente();
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();       
            if (rs.next()){    
                cl.setId(rs.getInt("ID"));
                cl.setCpf(rs.getLong("CPF"));
                cl.setCnpj(rs.getLong("CNPJ"));
                cl.setName(rs.getString("NOME"));
                cl.setTipo(rs.getInt("TIPO")==1 ? PESSOA_FISICA : PESSOA_JURIDICA);
                cl.setCadastro(rs.getDate("DATA_CADASTRO"));
                cl.setTelefone(rs.getString("TELEFONE")); 
                cl.setEndereco(EnderecoDB.getOne(rs.getInt("ENDERECO")));                
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return cl;
    }
    public static boolean delete(int id) {
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "delete from mr_cliente where ID = ?";
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        }
        /* Fazer as coisas caso tenha relaçao n para n*/
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static boolean virtualDelete(int id) {
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update mr_cliente set DELETADO = 1 where ID = ?";
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        }
        /* Fazer as coisas caso tenha relaçao n para n*/
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static boolean deleteFromEverything(int id) {
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        
        String sql11 = "select Viagem from mr_encomenda where MR_ENCOMENDA.REMETENTE = ? or MR_ENCOMENDA.DESTINATARIO = ?";
        String sql1 = "delete from mr_encomenda where REMETENTE = ? or DESTINATARIO = ?";
        String sql12 = "delete from mr_viagem where id = ?";
        String sql2 = "delete from mr_pagamento where  PAGADOR = ?";
        String sql3 = "delete from MR_COBRANCA where CLIENTE = ?";
        String sql4 = "select ENDERECO from mr_cliente where ID = ?";
        String sql5 = "delete from mr_cliente where ID = ?";
        String sql6 = "delete from MR_ENDERECO where ID = ?";
        int enderecoId;
        int viagem;
        try{
            PreparedStatement ps = c.prepareStatement(sql11);
            ps.setInt(1, id);
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                viagem = rs.getInt(1);
                
                ps = c.prepareStatement(sql1);
                ps.setInt(1, id);
                ps.setInt(2, id);
                ps.executeUpdate();

                ps = c.prepareStatement(sql12);
                ps.setInt(1, viagem);
                ps.executeUpdate();
            
                ps = c.prepareStatement(sql2);
                ps.setInt(1, id);
                ps.executeUpdate();

                ps = c.prepareStatement(sql3);
                ps.setInt(1, id);
                ps.executeUpdate();

                ps = c.prepareStatement(sql4);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                    enderecoId = rs.getInt(1);

                    ps = c.prepareStatement(sql5);
                    ps.setInt(1, id);
                    ps.executeUpdate();

                    ps = c.prepareStatement(sql6);
                    ps.setInt(1, enderecoId);
                    ps.executeUpdate();
                }
                
            }
            return true;
        }
        /* Fazer as coisas caso tenha relaçao n para n*/
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static boolean update(Cliente cliente){
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update mr_cliente set cpf = ?, cnpj = ?, nome = ?, tipo = ?, telefone = ? where id = ?";
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            if (cliente.getTipo() == PESSOA_FISICA){
                ps.setLong(1, cliente.getCpf());
                ps.setNull(2, Types.INTEGER);
            }
            else if(cliente.getTipo() == PESSOA_JURIDICA){
                ps.setNull(1, Types.INTEGER);
                ps.setLong(2, cliente.getCnpj());
            }
            ps.setString(3, cliente.getName());
            ps.setInt(4, Util.tipoClienteToInt(cliente.getTipo()));
            ps.setString(5, cliente.getTelefone());
            // A linha a seguir não faz o menor sentido.
            // ps.setInt(6, cliente.getEndereco().getId());
            // ---- 
            ps.setInt(6, cliente.getId());
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static boolean updateBasic(String nome,  int id, int telefone){
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update mr_cliente set  nome = ?, telefone = ? where id = ?";
        //EnderecoDB.update(c.getEndereco());//criar metodo
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            
            ps.setString(1, nome);
            ps.setInt(2, telefone);
            ps.setInt(3, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    public static boolean logicDelete(int id, int deletado){
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update mr_cliente set deletado = ? where id = ?";
        //EnderecoDB.update(c.getEndereco());//criar metodo
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            
            ps.setInt(1, deletado);
            ps.setInt(2, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    public static Cliente insert(Cliente c){
        Connection conn = new Conexao().getConexao();
        String sql = "insert into mr_cliente values (clientes.nextval, ?, ?, ?, ?, ?, ?, ?, ?)";
        EnderecoDB.insert(c.getEndereco());
        String genColumns[] = {"id"};
        try{
            PreparedStatement ps = conn.prepareStatement(sql, genColumns);
            if (c.getTipo() == TipoCliente.PESSOA_FISICA){
                ps.setLong(1, c.getCpf());
                ps.setNull(2, Types.INTEGER);
            }
            else{
                ps.setNull(1, Types.INTEGER);
                ps.setLong(2, c.getCnpj());
            }
            ps.setString(3, c.getName());
            ps.setInt(4, c.getTipo()== TipoCliente.PESSOA_FISICA? 1 : 2);
            ps.setString(5, c.getTelefone());
            c.setCadastro(new Date(new java.util.Date().getTime()));
            ps.setDate(6, c.getCadastro());
            ps.setInt(7, c.getEndereco().getId());
            ps.setInt(8, 0);
            int affected = ps.executeUpdate();
            ResultSet res = ps.getGeneratedKeys();
            if(affected == 1 && res.next()){
                c.setId(res.getInt(1));
                return c;
            }
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return null;
        }
        return null;
    }
}
