/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.database;

import ifrs.scali.model.Motorista;
import ifrs.scali.model.TipoUsuario;
import ifrs.scali.model.Util;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author Léo H.
 */
public class MotoristaDB {
    public static ArrayList<Motorista> getAll(){
        String selectSQL = "SELECT * FROM mr_motorista m " +
                           "INNER JOIN mr_funcionario u on m.id_funcionario = u.id";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement s = null;
        ArrayList <Motorista> motoristas = new ArrayList<>();
        try{
            s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Motorista u = new Motorista();
                u.setId(rs.getInt("ID"));
                u.setCpf(rs.getLong("CPF"));
                u.setNome(rs.getString("NOME"));
                u.setDataNascimento(rs.getDate("DATA_NASCIMENTO"));
                u.setNumHabilitacao(rs.getInt("NUM_HABILITACAO"));
                u.setCategorias(Util.categoriasFromString(rs.getString("CATEGORIAS")));
                u.setDataVencimento(rs.getDate("DATA_VENCIMENTO"));
                u.setDataHabilitacao(rs.getDate("DATA_HABILITACAO"));
                u.setId(rs.getInt("ID"));
                u.setUsername(rs.getString("USERNAME"));    
                u.setPassword(rs.getString("PASSWORD_HASH"));
                switch (rs.getInt("TIPO")) {
                    case 1:
                        u.setTipoUsuario(TipoUsuario.FUNCIONARIO);
                        break;
                    case 2:
                        u.setTipoUsuario(TipoUsuario.VENDEDOR);
                        break;
                    case 3:
                        u.setTipoUsuario(TipoUsuario.FINANCEIRO);
                        break;
                    case 4:
                        u.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
                        break;
                    case 5:
                        u.setTipoUsuario(TipoUsuario.MOTORISTA);
                        break; 
                }
                motoristas.add(u);
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return motoristas;
    }
    public static ArrayList<Motorista> getAllWhereClause(String clause){
        String selectSQL = "SELECT * FROM mr_motorista";
        clause = " WHERE " + clause;
        selectSQL = selectSQL + clause; 
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement s = null;
        ArrayList <Motorista> motoristas = new ArrayList<>();
        try{
            s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Motorista u = new Motorista();
                u.setId(rs.getInt("ID"));
                u.setCpf(rs.getLong("CPF"));
                u.setNome(rs.getString("NOME"));
                u.setDataNascimento(rs.getDate("DATA_NASCIMENTO"));
                u.setNumHabilitacao(rs.getInt("NUM_HABILITACAO"));
                u.setCategorias(Util.categoriasFromString(rs.getString("CATEGORIAS")));
                u.setDataVencimento(rs.getDate("DATA_VENCIMENTO"));
                u.setDataHabilitacao(rs.getDate("DATA_HABILITACAO"));
                u.setId(rs.getInt("ID"));
                u.setUsername(rs.getString("USERNAME"));    
                u.setPassword(rs.getString("PASSWORD_HASH"));
                switch (rs.getInt("TIPO")) {
                    case 1:
                        u.setTipoUsuario(TipoUsuario.FUNCIONARIO);
                        break;
                    case 2:
                        u.setTipoUsuario(TipoUsuario.VENDEDOR);
                        break;
                    case 3:
                        u.setTipoUsuario(TipoUsuario.FINANCEIRO);
                        break;
                    case 4:
                        u.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
                        break;
                    case 5:
                        u.setTipoUsuario(TipoUsuario.MOTORISTA);
                        break; 
                }
                motoristas.add(u);
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return motoristas;
    }
    
    public static ArrayList<Motorista> getAllLike(String text){
        String selectSQL = "SELECT * FROM mr_motorista WHERE CPF like ('%' + ? + '%') OR NOME like ('%' + ? + '%') ";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList <Motorista> motoristas = new ArrayList<>();
        try{
            PreparedStatement s = dbConnection.prepareStatement(selectSQL);
            s.setString(1, text);
            s.setString(2, text);
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Motorista u = new Motorista();
                u.setId(rs.getInt("ID"));
                u.setCpf(rs.getLong("CPF"));
                u.setNome(rs.getString("NOME"));
                u.setDataNascimento(rs.getDate("DATA_NASCIMENTO"));
                u.setNumHabilitacao(rs.getInt("NUM_HABILITACAO"));
                u.setCategorias(Util.categoriasFromString(rs.getString("CATEGORIAS")));
                u.setDataVencimento(rs.getDate("DATA_VENCIMENTO"));
                u.setDataHabilitacao(rs.getDate("DATA_HABILITACAO"));
                u.setId(rs.getInt("ID"));
                u.setUsername(rs.getString("USERNAME"));    
                u.setPassword(rs.getString("PASSWORD_HASH"));
                switch (rs.getInt("TIPO")) {
                    case 1:
                        u.setTipoUsuario(TipoUsuario.FUNCIONARIO);
                        break;
                    case 2:
                        u.setTipoUsuario(TipoUsuario.VENDEDOR);
                        break;
                    case 3:
                        u.setTipoUsuario(TipoUsuario.FINANCEIRO);
                        break;
                    case 4:
                        u.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
                        break;
                    case 5:
                        u.setTipoUsuario(TipoUsuario.MOTORISTA);
                        break; 
                }
                motoristas.add(u);
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return motoristas;
    }
    
    
    public static Motorista getOne(int id){
        String selectSQL = "SELECT * FROM mr_motorista m " +
                           "INNER JOIN mr_funcionario u on m.id_funcionario = u.id where m.id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();       
            if (rs.next()){    
                Motorista u = new Motorista();
                u.setId(rs.getInt("ID"));
                u.setCpf(rs.getLong("CPF"));
                u.setNome(rs.getString("NOME"));
                u.setDataNascimento(rs.getDate("DATA_NASCIMENTO"));
                u.setNumHabilitacao(rs.getInt("NUM_HABILITACAO"));
                u.setCategorias(Util.categoriasFromString(rs.getString("CATEGORIAS")));
                u.setDataVencimento(rs.getDate("DATA_VENCIMENTO"));
                u.setDataHabilitacao(rs.getDate("DATA_HABILITACAO"));
                u.setId(rs.getInt("ID"));
                u.setUsername(rs.getString("USERNAME"));    
                u.setPassword(rs.getString("PASSWORD_HASH"));
                switch (rs.getInt("TIPO")) {
                    case 1:
                        u.setTipoUsuario(TipoUsuario.FUNCIONARIO);
                        break;
                    case 2:
                        u.setTipoUsuario(TipoUsuario.VENDEDOR);
                        break;
                    case 3:
                        u.setTipoUsuario(TipoUsuario.FINANCEIRO);
                        break;
                    case 4:
                        u.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
                        break;
                    case 5:
                        u.setTipoUsuario(TipoUsuario.MOTORISTA);
                        break; 
                }
                return u;
            }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    public static boolean delete(int id) {
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "delete from mr_motorista where ID = ?";
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        }
        /* Fazer as coisas caso tenha relaçao n para n*/
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static boolean update(int cpf, String nome, boolean categorias[], int id, int numHabilitacao){
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update mr_motorista set cpf = ?,  nome = ?, categorias = ?, NUM_HABILITACAO = ? where id = ?";
         
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, cpf);
            ps.setString(2, nome);
            ps.setString(3, Util.categoriasToString(categorias));
            ps.setInt(4, numHabilitacao);
            ps.setInt(5, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static boolean updateBasic(String nome,  int id, int telefone){
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update mr_motorista set  nome = ? where id = ?";
        //EnderecoDB.update(c.getEndereco());//criar metodo
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            
            ps.setString(1, nome);
            ps.setInt(3, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static boolean logicDelete(int id, int deletado){
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update mr_motorista set deletado = ? where id = ?";
        //EnderecoDB.update(c.getEndereco());//criar metodo
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            
            ps.setInt(1, deletado);
            ps.setInt(2, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static boolean insert(Motorista m){
        Connection conn = new Conexao().getConexao();
        int idFuncionario = FuncionarioDB.insert(m).getId();
        String sql = "insert into mr_motorista values (motoristas.nextval, ?, ?, ?, ?, ?, ?)";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, m.getNumHabilitacao());
            ps.setString(2,  Util.categoriasToString(m.getCategorias()));
            ps.setDate(3, m.getDataVencimento());
            ps.setDate(4, m.getDataHabilitacao());
            ps.setInt(5, 0);
            ps.setInt(6, idFuncionario);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
}
