/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.database;

import ifrs.scali.model.Veiculo;
import ifrs.scali.model.Viagem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Léo H.
 */
public class ViagemDB {
    public static List<Viagem> getAllVeiculos(){
        String selectSQL = "SELECT * FROM MR_VIAGEM";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement s = null;
        ArrayList <Viagem> viagens = new ArrayList<Viagem>();
        try{
            s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Viagem cl = new Viagem();
                cl.setId(rs.getInt("ID"));
                cl.setVeiculo(VeiculoDB.getOne(rs.getInt("VEICULO")));
                cl.setPartida(rs.getDate("PARTIDA"));
                cl.setChegada(rs.getDate("CHEGADA"));
                viagens.add(cl);    
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return viagens;
    }
    
    public static boolean insert(Viagem m){
        Connection conn = new Conexao().getConexao();
        String sql = "insert into MR_VIAGEM values (viagem.nextval, ?, ?, ?)";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, m.getVeiculo().getId());
            ps.setDate(2, m.getPartida());
            ps.setDate(3, m.getChegada());
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
     public static boolean delete(int id) {
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "delete from MR_VIAGEM where ID = ?";
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static Viagem getOne(int id){
        String selectSQL = "SELECT * FROM MR_VIAGEM where id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;    
        Viagem cl = new Viagem();
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();       
            if (rs.next()){    
                cl.setId(rs.getInt("ID"));
                cl.setVeiculo(VeiculoDB.getOne(rs.getInt("VEICULO")));
                cl.setPartida(rs.getDate("PARTIDA"));
                cl.setChegada(rs.getDate("CHEGADA"));
            }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return cl;
    }
}
