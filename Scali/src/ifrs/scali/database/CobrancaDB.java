/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.database;

import ifrs.scali.model.Cobranca;
import ifrs.scali.model.Motorista;
import ifrs.scali.model.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Léo H.
 */
public class CobrancaDB {
    public static ArrayList<Cobranca> getAll(){
        String selectSQL = "SELECT * FROM mr_cobranca";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement s = null;
        ArrayList <Cobranca> cobrancas = new ArrayList<Cobranca>();
        try{
            s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Cobranca co = new Cobranca();
                co.setId(rs.getInt("ID"));
                co.setData(rs.getDate("DATA"));
                co.setValorParcela(rs.getDouble("VALOR_PARCELA"));
                co.setDiaMesPagamento(rs.getInt("DIA_MES_PAGAMENTO"));
                co.setNumParcelas(rs.getInt("NUM_PARCELAS"));
                co.setCliente(ClienteDB.getOne(rs.getInt("ID")));
                co.setIsCotacao(rs.getBoolean("ISCOTACAO"));
                cobrancas.add(co);    
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return cobrancas;
    }
    public static ArrayList<Cobranca> getAllWhereClause(String clause){
        String selectSQL = "SELECT * FROM mr_cobranca";
        clause = " WHERE " + clause;
        selectSQL = selectSQL + clause; 
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement s = null;
        ArrayList <Cobranca> cobrancas = new ArrayList<Cobranca>();
        try{
            s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Cobranca co = new Cobranca();
                co.setId(rs.getInt("ID"));
                co.setData(rs.getDate("DATA"));
                co.setValorParcela(rs.getDouble("VALOR_PARCELA"));
                co.setDiaMesPagamento(rs.getInt("DIA_MES_PAGAMENTO"));
                co.setNumParcelas(rs.getInt("NUM_PARCELAS"));
                co.setCliente(ClienteDB.getOne(rs.getInt("ID")));
                co.setIsCotacao(rs.getBoolean("ISCOTACAO"));
                cobrancas.add(co);
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return cobrancas;
    }
    
    public static ArrayList<Cobranca> getAllLike(String text){
        String selectSQL = "SELECT * FROM mr_cobranca WHERE VALOR_PARCELA like ('%' + ? + '%') ";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList <Cobranca> cobrancas = new ArrayList<Cobranca>();
        try{
            PreparedStatement s = dbConnection.prepareStatement(selectSQL);
            s.setString(1, text);
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Cobranca co = new Cobranca();
                co.setId(rs.getInt("ID"));
                co.setData(rs.getDate("DATA"));
                co.setValorParcela(rs.getDouble("VALOR_PARCELA"));
                co.setDiaMesPagamento(rs.getInt("DIA_MES_PAGAMENTO"));
                co.setNumParcelas(rs.getInt("NUM_PARCELAS"));
                co.setCliente(ClienteDB.getOne(rs.getInt("ID")));
                co.setIsCotacao(rs.getBoolean("ISCOTACAO"));
                cobrancas.add(co);    
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return cobrancas;
    }
    
    
    public static Cobranca getOne(int id){
        String selectSQL = "SELECT * FROM MR_COBRANCA where id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;    
        Cobranca co = new Cobranca();
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();       
            if (rs.next()){    
                co.setId(rs.getInt("ID"));
                co.setData(rs.getDate("DATA"));
                co.setValorParcela(rs.getDouble("VALOR_PARCELA"));
                co.setDiaMesPagamento(rs.getInt("DIA_MES_PAGAMENTO"));
                co.setNumParcelas(rs.getInt("NUM_PARCELAS"));
                co.setCliente(ClienteDB.getOne(rs.getInt("ID")));
                co.setIsCotacao(rs.getBoolean("ISCOTACAO"));
            }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return co;
    }
    public static boolean delete(int id) {
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "delete from mr_cobranca where ID = ?";
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        }
        /* Fazer as coisas caso tenha relaçao n para n*/
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static boolean update(double valorParcela, int diaMesPagamento, int numParcelas , boolean isCotacao, int id){
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update MR_COBRANCA set valor_parcela = ?,  DIA_MES_PAGAMENTO = ?, NUM_PARCELAS = ?, ISCOTACAO = ? where id = ?";
         
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setDouble(1, valorParcela);
            ps.setInt(2, diaMesPagamento);
            ps.setInt(3, numParcelas);
            ps.setBoolean(4, isCotacao);
            ps.setInt(5, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
  
    
    public static boolean updateIsCotacao(int id, boolean isCotacao){
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update mr_motorista set ISCOTACAO = ? where id = ?";
        //EnderecoDB.update(c.getEndereco());//criar metodo
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            
            ps.setBoolean(1, isCotacao);
            ps.setInt(2, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    public static boolean insert(Cobranca co){
        Connection conn = new Conexao().getConexao();
        String sql = "insert into MR_COBRANCA values (cobrancas .nextval, ?, ?, ?, ?, ? , ?)";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setDate(1, co.getData());
            ps.setDouble(2, co.getValorParcela());
            ps.setInt(3, co.getDiaMesPagamento());
            ps.setInt(4, co.getNumParcelas());
            ps.setInt(5, co.getCliente().getId());
            ps.setInt(6, 0);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
     public static boolean logicDelete(int id, int deletado){
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update MR_COBRANCA set deletado = ? where id = ?";
        //EnderecoDB.update(c.getEndereco());//criar metodo
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            
            ps.setInt(1, deletado);
            ps.setInt(2, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
}
