/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.database;

import ifrs.scali.model.Funcionario;
import ifrs.scali.model.Motorista;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import ifrs.scali.model.TipoUsuario;
import ifrs.scali.model.Util;
import java.sql.PreparedStatement;

/**
 *
 * @author Raynan P.
 */
public class FuncionarioDB {

    public static ArrayList<Funcionario> getAllWhereClause(String clause) {
        String selectSQL = "SELECT * FROM mr_funcionario";
        selectSQL = selectSQL + " " + clause;
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Funcionario> usuarios = new ArrayList<>();
        try {
            Statement s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()) {
                Funcionario u = new Funcionario();
                u.setId(rs.getInt("ID"));
                u.setUsername(rs.getString("USERNAME"));
                u.setPassword(rs.getString("PASSWORD_HASH"));
                u.setNome(rs.getString("NOME"));
                u.setCpf(rs.getLong("CPF"));
                u.setDataNascimento(rs.getDate("DATA_NASCIMENTO"));
                switch (rs.getInt("TIPO")) {
                    case 1:
                        u.setTipoUsuario(TipoUsuario.FUNCIONARIO);
                        break;
                    case 2:
                        u.setTipoUsuario(TipoUsuario.VENDEDOR);
                        break;
                    case 3:
                        u.setTipoUsuario(TipoUsuario.FINANCEIRO);
                        break;
                    case 4:
                        u.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
                        break;
                    case 5:
                        u.setTipoUsuario(TipoUsuario.MOTORISTA);
                        break;
                }
                usuarios.add(u);
            }
            c.desconecta();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return usuarios;

    }

    public static ArrayList<Funcionario> getAll() {
        String selectSQL = "SELECT * FROM mr_funcionario";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Funcionario> usuarios = new ArrayList<>();
        try {
            Statement s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()) {
                Funcionario u = new Funcionario();
                u.setId(rs.getInt("ID"));
                u.setUsername(rs.getString("USERNAME"));
                u.setPassword(rs.getString("PASSWORD_HASH"));
                u.setNome(rs.getString("NOME"));
                u.setCpf(rs.getLong("CPF"));
                u.setDataNascimento(rs.getDate("DATA_NASCIMENTO"));
                switch (rs.getInt("TIPO")) {
                    case 1:
                        u.setTipoUsuario(TipoUsuario.FUNCIONARIO);
                        break;
                    case 2:
                        u.setTipoUsuario(TipoUsuario.VENDEDOR);
                        break;
                    case 3:
                        u.setTipoUsuario(TipoUsuario.FINANCEIRO);
                        break;
                    case 4:
                        u.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
                        break;
                    case 5:
                        u.setTipoUsuario(TipoUsuario.MOTORISTA);
                        break;
                }
                usuarios.add(u);
            }
            c.desconecta();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return usuarios;
    }

    public static Funcionario autenticar(String usuarioEnviado, String senhaEnviada) {
        String selectSQL = "SELECT * FROM mr_funcionario where USERNAME = ?";
        Conexao c = new Conexao();
        senhaEnviada = Util.criptografarSenha(usuarioEnviado, senhaEnviada);
        Connection dbConnection = c.getConexao();
        Funcionario u = null;
        try {
            PreparedStatement ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, usuarioEnviado);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                u = new Funcionario();
                u.setId(rs.getInt("ID"));
                u.setUsername(rs.getString("USERNAME"));    
                u.setPassword(rs.getString("PASSWORD_HASH"));
                u.setNome(rs.getString("NOME"));
                u.setCpf(rs.getLong("CPF"));
                u.setDataNascimento(rs.getDate("DATA_NASCIMENTO"));
                switch (rs.getInt("TIPO")) {
                    case 1:
                        u.setTipoUsuario(TipoUsuario.FUNCIONARIO);
                        break;
                    case 2:
                        u.setTipoUsuario(TipoUsuario.VENDEDOR);
                        break;
                    case 3:
                        u.setTipoUsuario(TipoUsuario.FINANCEIRO);
                        break;
                    case 4:
                        u.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
                        break;
                    case 5:
                        u.setTipoUsuario(TipoUsuario.MOTORISTA);
                        break; 
                }
            }
            c.desconecta();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if(u != null && u.getPassword().equals(senhaEnviada)){
            return u;
        } else {
            return null;
        }
    }

    public static Funcionario getOne(int id) {
        String selectSQL = "SELECT * FROM mr_funcionario where id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Funcionario u = new Funcionario();
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                u.setId(rs.getInt("ID"));
                u.setUsername(rs.getString("USERNAME"));
                u.setPassword(rs.getString("PASSWORD_HASH"));
                u.setNome(rs.getString("NOME"));
                u.setCpf(rs.getLong("CPF"));
                u.setDataNascimento(rs.getDate("DATA_NASCIMENTO"));
                switch (rs.getInt("TIPO")) {
                    case 1:
                        u.setTipoUsuario(TipoUsuario.FUNCIONARIO);
                        break;
                    case 2:
                        u.setTipoUsuario(TipoUsuario.VENDEDOR);
                        break;
                    case 3:
                        u.setTipoUsuario(TipoUsuario.FINANCEIRO);
                        break;
                    case 4:
                        u.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
                        break;
                    case 5:
                        u.setTipoUsuario(TipoUsuario.MOTORISTA);
                        break;
                }
            }
            c.desconecta();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return u;
    }

    public static boolean logicDelete(int id, int deletado) {
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "update mr_funcionario set deletado = ? where id = ?";
        try {
            PreparedStatement ps = c.prepareStatement(sql);

            ps.setInt(1, deletado);
            ps.setInt(2, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    // TODO.
    public static Funcionario insert(Funcionario m){
        Connection conn = new Conexao().getConexao();
        String sql = "insert into MR_FUNCIONARIO values (FUNCIONARIO.nextval, ?, ?, ?, ?, ?, ?, ?)";
        String generatedColumns[] = {"id"};
        try{
            PreparedStatement ps = conn.prepareStatement(sql, generatedColumns);
            ps.setString(1, m.getUsername());
            ps.setInt(2, 0);
            ps.setString(3, Util.criptografarSenha(m.getUsername(), m.getPassword()));
            ps.setInt(4, Util.tipoUsuarioToInt(m.getTipoUsuario()));
            ps.setString(5, m.getNome());
            ps.setLong(6, m.getCpf());
            ps.setDate(7, m.getDataNascimento());
            int affected = ps.executeUpdate();
            ResultSet res = ps.getGeneratedKeys();
            if(affected == 1 && res.next()){
                m.setId(res.getInt(1));
                return m;
            }
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return null;
        }
        return null;
    }
}
