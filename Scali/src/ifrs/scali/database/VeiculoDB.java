/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.database;

import ifrs.scali.model.Veiculo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Léo H.
 */
public class VeiculoDB {
   public static List<Veiculo> getAllVeiculos(){
        String selectSQL = "SELECT * FROM MR_VEICULO";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList <Veiculo> veiculos = new ArrayList<>();
        try{
            Statement s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                Veiculo cl = new Veiculo();
                cl.setId(rs.getInt("ID"));
                cl.setPlaca(rs.getString("PLACA"));
                cl.setDescricao(rs.getString("DESCRICAO"));
                cl.setTipo(TipoVeiculoDB.getOne(rs.getInt("TIPO")));
                cl.setRenavam(rs.getInt("RENAVAM"));
                cl.setDataCadastro(rs.getDate("DATA_CADASTRO"));
                cl.setKmAtual(rs.getInt("KM_ATUAL"));
                cl.setNumeroChassi(rs.getString("NUMERO_CHASSI"));
                cl.setAnoFabricacao(rs.getInt("ANO_FABRICACAO"));
                cl.setCapacidadeM3(rs.getInt("CAPACIDADE_M3"));
                cl.setCapacidadeKg(rs.getInt("CAPACIDADE_KG"));
                cl.setTaraKg(rs.getInt("TARA_KG"));
                cl.setTipoCombustivel(TipoCombustivelDB.getOne(rs.getInt("TIPO_COMBUSTIVEL")));
                cl.setMotorista(MotoristaDB.getOne(rs.getInt("MOTORISTA")));
                veiculos.add(cl);    
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return veiculos;
    }
    
    public static boolean insert(Veiculo m){
        Connection conn = new Conexao().getConexao();
        String sql = "insert into MR_VEICULO values (veiculo.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, m.getPlaca());
            ps.setString(2, m.getDescricao());
            ps.setInt(3, m.getTipo().getId());
            ps.setLong(4, m.getRenavam());
            ps.setDate(5, m.getDataCadastro());
            ps.setInt(6, m.getKmAtual());
            ps.setString(7, m.getNumeroChassi());
            ps.setInt(8, m.getAnoFabricacao());
            ps.setDouble(9, m.getCapacidadeM3());
            ps.setDouble(10, m.getCapacidadeKg());
            ps.setDouble(11, m.getTaraKg());
            ps.setInt(12, m.getTipoCombustivel().getId());
            ps.setInt(13, m.getMotorista().getId());
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
     public static boolean delete(int id) {
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "delete from MR_VEICULO where ID = ?";
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static Veiculo getOne(int id){
        String selectSQL = "SELECT * FROM MR_VEICULO where id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;    
        Veiculo cl = new Veiculo();
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();       
            if (rs.next()){    
                
                cl.setId(rs.getInt("ID"));
                cl.setPlaca(rs.getString("PLACA"));
                cl.setDescricao(rs.getString("DESCRICAO"));
                cl.setTipo(TipoVeiculoDB.getOne(rs.getInt("TIPO")));
                cl.setRenavam(rs.getInt("RENAVAM"));
                cl.setDataCadastro(rs.getDate("DATA_CADASTRO"));
                cl.setKmAtual(rs.getInt("KM_ATUAL"));
                cl.setNumeroChassi(rs.getString("NUMERO_CHASSI"));
                cl.setAnoFabricacao(rs.getInt("ANO_FABRICACAO"));
                cl.setCapacidadeM3(rs.getInt("CAPACIDADE_M3"));
                cl.setCapacidadeKg(rs.getInt("CAPACIDADE_KG"));
                cl.setTaraKg(rs.getInt("TARA_KG"));
                cl.setTipoCombustivel(TipoCombustivelDB.getOne(rs.getInt("TIPO_COMBUSTIVEL")));
                cl.setMotorista(MotoristaDB.getOne(rs.getInt("MOTORISTA")));
            }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return cl;
    }
}
