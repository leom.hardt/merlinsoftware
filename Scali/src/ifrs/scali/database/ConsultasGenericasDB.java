/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.database;

import ifrs.scali.model.Cliente;
import static ifrs.scali.model.Cliente.TipoCliente.PESSOA_FISICA;
import static ifrs.scali.model.Cliente.TipoCliente.PESSOA_JURIDICA;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Raynan P.
 */
public class ConsultasGenericasDB {
    public static int getNumberOf(String clause){
        String selectSQL = clause;
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement s = null;
        int number=0;
        try{
            s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            if (rs.next()){
                number = rs.getInt(1);
            }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return number;
    }
}