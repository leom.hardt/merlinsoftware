/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.database;

import ifrs.scali.model.TipoCombustivel;
import ifrs.scali.model.TipoVeiculo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class TipoCombustivelDB {
    public static List<TipoCombustivel> getAllTipoCombustivel(){
        String selectSQL = "SELECT * FROM MR_TIPO_COMBUSTIVEL";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement s = null;
        ArrayList <TipoCombustivel> tipo = new ArrayList<TipoCombustivel>();
        try{
            s = dbConnection.createStatement();
            ResultSet rs = s.executeQuery(selectSQL);
            while (rs.next()){               
                TipoCombustivel cl = new TipoCombustivel();
                cl.setId(rs.getInt("ID"));
                cl.setDescricao(rs.getString("DESCRICAO"));
                tipo.add(cl);    
              }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return tipo;
    }
    
    public static boolean insert(TipoCombustivel m, int id){
        Connection conn = new Conexao().getConexao();
        String sql = "insert into MR_TIPO_COMBUSTIVEL values (?, ?)";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, m.getDescricao());
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
     public static boolean delete(int id) {
        Conexao con = new Conexao();
        Connection c = con.getConexao();
        String sql = "delete from MR_TIPO_COMBUSTIVEL where ID = ?";
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public static TipoCombustivel getOne(int id){
        String selectSQL = "SELECT * FROM MR_TIPO_COMBUSTIVEL where id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;    
        TipoCombustivel m = new TipoCombustivel();
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();       
            if (rs.next()){    
                m.setId(rs.getInt("ID"));
                m.setDescricao(rs.getString("DESCRICAO"));
            }
            c.desconecta();
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return m;
    }
}
