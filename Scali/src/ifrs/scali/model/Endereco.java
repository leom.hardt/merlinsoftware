/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.model;

/**
 *
 * @author Léo H.
 */
public class Endereco {
    private int id;
    private String cep;
    private String logradouro;
    private int numero;
    private String cidade;
    private String estado;
    private String complemento;
    private String bairro;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }
    
    @Override
    public String toString(){
        String result = "Endereço[";
        result += "\n\tid: " + id;
        result += "\n\tcep: " + cep;
        result += "\n\tlogradouro: " + logradouro;
        result += "\n\tnumero: " + numero;
        result += "\n\tcidade: " + cidade;
        result += "\n\testado: " + estado;
        result += "\n\tcomplemento: " + complemento;
        result += "\n\tbairro: " + bairro;
        result += "\n];";
        return result;
    }
    
    public String resumido(){
        return logradouro + ", " + numero;
    }
}
