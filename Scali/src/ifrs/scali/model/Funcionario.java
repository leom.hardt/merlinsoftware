/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.model;

import java.sql.Date;

/**
 *
 * @author Léo Hardt
 */
public class Funcionario {
    private int id;
    private String username;
    private String password;
    private TipoUsuario tipoUsuario;
    
    
    private String nome;
    private long cpf;
    private Date dataNascimento;
    
    public Funcionario(){
        
    }
    
    /* Copia os valores de outro funcionário */
    public Funcionario(Funcionario f){ 
        id = f.id;
        username = f.username;
        password = f.password;
        tipoUsuario = f.tipoUsuario;
        nome = f.nome;
        cpf = f.cpf;
        dataNascimento = f.dataNascimento;
    }

    public int getId() {
        return id;
    }

    public String getDocumento(){
        return Util.cpfFromLong(cpf);
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
    
    public String getTipoUsuarioStr(){
        switch(tipoUsuario){
            case FUNCIONARIO:
                return "Funcionário";
            case VENDEDOR:
                return "Vendedor";
            case FINANCEIRO:
                return "Financeiro";
            case ADMINISTRADOR:
                return "Administrador";
            case MOTORISTA:
                return "Motorista";
        }
        return "Erro!";
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    
    
}
