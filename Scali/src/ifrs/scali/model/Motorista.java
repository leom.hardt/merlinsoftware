/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.model;

import java.sql.Date;

/**
 *
 * @author Léo H.
 */
public class Motorista extends Funcionario {
    long numHabilitacao;
    boolean categorias[];
    Date dataVencimento;
    Date dataHabilitacao;
    
    public Motorista(){
        
    }
    
    /* Cria um motorista a partir de um funcionário */
    public Motorista(Funcionario f){
        super(f);
    }
    
    public long getNumHabilitacao() {
        return numHabilitacao;
    }

    public void setNumHabilitacao(long numHabilitacao) {
        this.numHabilitacao = numHabilitacao;
    }

    public boolean[] getCategorias() {
        return categorias;
    }

    public void setCategorias(boolean[] categorias) {
        this.categorias = categorias;
    }
    
    public void setCategorias(boolean a, boolean b, boolean c, boolean d, boolean e){
        categorias = new boolean[5];
        categorias[0] = a;
        categorias[1] = b;
        categorias[2] = c;
        categorias[3] = d;
        categorias[4] = e;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Date getDataHabilitacao() {
        return dataHabilitacao;
    }

    public void setDataHabilitacao(Date dataHabilitacao) {
        this.dataHabilitacao = dataHabilitacao;
    }
    
    
    public String getVencimentoStr(){
        return Util.dateToString(dataVencimento);
    }
    
    public String getCategoriasStr(){
        return Util.categoriasToString(categorias);
    }
    
}
