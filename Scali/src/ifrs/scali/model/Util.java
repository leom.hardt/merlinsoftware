/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.model;

import ifrs.scali.model.Cliente.TipoCliente;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.sql.Date;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;

/**
 *
 * @author Léo H.
 */
public class Util {

    public static int tipoUsuarioToInt(TipoUsuario t){
        switch(t){
            case FUNCIONARIO:
                return 1;
            case VENDEDOR:
                return 2;   
            case FINANCEIRO:
                return 3;
            case ADMINISTRADOR:
                return 4;
            case MOTORISTA:
                return 5;
        }
        return -1;
    }
    
    public static int tipoClienteToInt(TipoCliente t){
        switch(t){
            case PESSOA_FISICA:
                return 1;
            case PESSOA_JURIDICA:
                return 2;
        }
        return -1;
    }
    
    public static boolean isTelephoneValid(String telephone){
        if(telephone != null){
            telephone = telephone.replaceAll("[^0-9]+", "");
            // 1º Caso +55 51 99999-9999 -> 13 díg.
            // 2º Caso +55 51  3999-9999 -> 12 díg.
            // 3º Caso     51 99999-9999 -> 11 díg.
            // 4º Caso     51  3999-9999 -> 10 díg.
            // 3º Caso        99999-9999 ->  9 díg.
            // 4º Caso         3999-9999 ->  8 díg.
            return (telephone.length() >= 8 && telephone.length() <= 13);
        }
        return false;
    }
    
    /**
     * Confirma formatação e dígitos verificadores de um CPF.
     * @param cpf o CPF a ser validado.
     * @return true se o CPF for válido.
     */
    public static boolean isCpfValid(String cpf){
        int soma = 0, v1 = 0, v2 = 0;
        if(cpf != null){
            // Remove tudo que não é dígito
            cpf = cpf.replaceAll("[^0-9]+", "");
            int digitos[] = new int[11];
            if(cpf.length() == 11){
                for(int i = 0; i < 11; i++){
                    if(Character.isDigit(cpf.charAt(i))){
                        digitos[i] = Character.getNumericValue(cpf.charAt(i));
                    } else return false;
                }
                // Se todos os dígitos forem iguais, falso
                int ctrEqual = 0;
                for(int i = 1; i < 9; i++){
                    if(digitos[i] == digitos[0])
                        ctrEqual++;
                }
                if(ctrEqual == 8) return false;
                
                soma = 0;
                for(int i = 0; i < 9; i++){
                    soma += digitos[i] * (10-i);
                }
                
                v1 = 11 - (soma % 11);
                if (v1 == 10 || v1 == 11){
                    v1 = 0;	
                }		
                if(v1 != digitos[9]) {		
                    return false;	
                }
                soma = 0;	
                for (int i = 0; i < 10; i ++){
                    soma += digitos[i] * (11 - i);	
                }		
                v2 = 11 - (soma % 11);	
                if (v2 == 10 || v2 == 11){
                    v2 = 0;	
                }
                
                return (v2 == digitos[10]);
            }
        }
	return false;
    }

    /**
     * Confirma formatação e dígitos verficadores de um CNPJ.
     * @param cnpj o CNPJ a ser validado.
     * @return true se o CNPJ é válido.
     */
    public static boolean isCnpjValid(String cnpj){
        if(cnpj != null){
            cnpj = cnpj.replaceAll("[^0-9]+", "");
            if(cnpj.length() == 14){
                int digitos[] = new int[14];
                // Truque dos zeros para não ter que usar outro for.
                int mult1[] = {5,4,3,2,9,8,7,6,5,4,3,2,0,0};
                int mult2[] = {6,5,4,3,2,9,8,7,6,5,4,3,2,0};
                int total1 = 0, total2 = 0;
                for(int i = 0; i < 14; i++){
                    digitos[i] = Character.getNumericValue(cnpj.charAt(i));
                    total1 += digitos[i] * mult1[i];
                    total2 += digitos[i] * mult2[i]; 
                }
                
                total1 = total1 % 11;
                total2 = total2 % 11;
                
                if(total1 < 2){ 
                    total1 = 0;
                }else {
                   total1 = 11 - total1;
                }
      
                if(total2 < 2){ 
                    total2 = 0;
                }else {
                   total2 = 11 - total2;
                }
                
                return (total1 == digitos[12]) 
                    && (total2 == digitos[13]);
            }
            
        }
        return false;
    }
    
    /**
     * Retorna se o CEP foi formatado corretamente.
     * @return Se a string está no formato NNNNN-NNN
     */
    public static boolean isCepValid(String cep){
        return cep.matches("[0-9][0-9][0-9][0-9][0-9]\\-[0-9][0-9][0-9]");
    }
    
    public static String cpfFromLong(long cpf){
        long digito = 0;
        String str = "";
        
        for(int i = 10; i >= 0; i--){
            digito =  cpf%10;
            cpf -= digito;
            cpf = cpf/10;
            str = digito + str;
        }
        str = str.substring(0,3) + "." + str.substring(3,6) 
              + "." + str.substring(6,9) + "-" + str.substring(9);
        return str;
    }
    
    public static String cnpjFromLong(long cnpj){
        long digito = 0;
        String str = "";
        
        for(int i = 13; i >= 0; i--){
            digito =  cnpj%10;
            cnpj -= digito;
            cnpj = cnpj/10;
            str = digito + str;
        }
        str = str.substring(0,2) + "." + str.substring(2,5) 
              + "." + str.substring(5,8) + "/" + str.substring(8,12)
              + "-" + str.substring(12);
        return str;
    }
    
    public static String cepFromLong(long cep){
        long digito = 0;
        String str = "";
        
        for(int i = 7; i >= 0; i--){
            digito =  cep%10;
            cep -= digito;
            cep = cep/10;
            str = digito + str;
        }
        str = str.substring(0,5) + "-" + str.substring(5);
        return str;
    }
    
    public static long cpfToLong(String cpf){
        if(isCpfValid(cpf)){
            if(cpf != null){
                // Remove tudo que não é dígito
                cpf = cpf.replaceAll("[^0-9]+", "");
                int digitos[] = new int[11];
                if(cpf.length() == 11){
                    for(int i = 0; i < 11; i++){
                        if(Character.isDigit(cpf.charAt(i))){
                            digitos[i] = Character.getNumericValue(cpf.charAt(i));
                        } else return -1;
                    }
                }
                long mult = 1, cpfLong = 0;
                for(int i = 10; i >= 0; i--){
                    cpfLong += digitos[i] * mult; 
                    mult *= 10;
                }
                return cpfLong;
            }
        }
        return -1;
    }
    
    public static long cnpjToLong(String cnpj){
        if(isCnpjValid(cnpj)){
            if(cnpj != null){
                // Remove tudo que não é dígito
                cnpj = cnpj.replaceAll("[^0-9]+", "");
                int digitos[] = new int[14];
                if(cnpj.length() == 14){
                    for(int i = 0; i < 14; i++){
                        if(Character.isDigit(cnpj.charAt(i))){
                            digitos[i] = Character.getNumericValue(cnpj.charAt(i));
                        } else return -1;
                    }
                }
                long mult = 1, cpfLong = 0;
                for(int i = 13; i >= 0; i--){
                    cpfLong += digitos[i] * mult; 
                    mult *= 10;
                }
                return cpfLong;
            }
        }
        return -1;
    }
    
    /**
     * Retorna a conversão de uma string de CEP para o long correspondente.
     * 
     * @param cep um cep válido, segundo a função isCepValid;
     * @return a conversão de uma string de CEP para o long correspondente.
     */
    public static long cepToLong(String cep){
        if(isCepValid(cep)){
            if(cep != null){
                // Remove tudo que não é dígito
                cep = cep.replaceAll("[^0-9]+", "");
                int digitos[] = new int[8];
                if(cep.length() == 8){
                    for(int i = 0; i < 8; i++){
                        if(Character.isDigit(cep.charAt(i))){
                            digitos[i] = Character.getNumericValue(cep.charAt(i));
                        } else return -1;
                    }
                }
                long mult = 1, cepLong = 0;
                for(int i = 7; i >= 0; i--){
                    cepLong += digitos[i] * mult; 
                    mult *= 10;
                }
                return cepLong;
            }
             
        }
        return -1;
    }
    
    /** Criptografa uma senha com SHA-256.
     *
     * @author: Léo H.
     * 
     * @param usuario: O nome de usuário.
     * @param senha: A senha do usuário.
     * 
     * @return o hash da senha com o nome de usuário.
     * 
     */
    public static String criptografarSenha(String usuario, String senha){
        try{
            MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
            senha = senha + "@" + usuario;
            byte messageDigest[] = algorithm.digest(senha.getBytes("UTF-8"));

            StringBuilder hex = new StringBuilder(messageDigest.length * 2);
            for(byte b : messageDigest){
              hex.append(String.format("%02x", b));
            }
            return hex.toString();      
        } catch(Exception ex){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Algorítmo de criptografia falhando");
            Label labelErro = new Label(ex + "");
            labelErro.setWrapText(true);
            alert.getDialogPane().setContent(labelErro);
            ex.printStackTrace();
            
            alert.showAndWait();
        }
        return "";
    }
    
    public static ArrayList<String> getAllEstados(){
        return new ArrayList<String>(Arrays.asList(
            "AC", "AL", "AP", "AM", "BA", "CE",
            "DF", "ES", "GO", "MA", "MT", "MS",
            "MG", "PA", "PB", "PR", "PE", "PI",
            "RJ", "RN", "RS", "RO", "RR", "SC",
            "SP", "SE", "TO"
        ));
    }
    
    /**
     * Vê se a sigla é correspondente a um estado brasileiro.
     * @param estado Uma sigla de estado. Por exemplo, "RS".
     * @return verdadeiro se uma sigla de estado é válida.
     */
    public static boolean isEstadoValid(String estado){
        return (getAllEstados().contains(estado.toUpperCase()));
    }
    
    /**
     * Busca um endereço online, baseado em uma String cep válida.
     * @param cep Um cep válido.
     * @return O endereço correspondente ao cep, vindo da Internet.
     */
    public static Endereco buscarPorCepOnline(String cep){
        Endereco endereco = new Endereco();
        endereco.setId(-1);
        try{
            // Tira todo caractere não numérico
            cep = cep.replace("[^0-9]+", "");
            URL url = new URL("https://viacep.com.br/ws/" + cep + "/json/?callback=callback_name");
            URLConnection conn = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            // Enquanto houver texto para ser lido, ele tentará encaixar em algum dos parâmetros.
            // Faz isso vendo se a linha dada, por exemplo <      "cep": "92310-300">, ele tentará
            // selecionará tudo após o sinal de dois pontos e filtrará aspas e espaços
            while(in.ready()){
                String retornoValues[] = (in.readLine()).split(":");
                if(retornoValues[0].matches("[ \t]+\"cep\"")){
                    endereco.setCep(retornoValues[1].replaceAll("[ ,\"]",""));
                } else if(retornoValues[0].matches("[ \t]+\"logradouro\"")){
                    endereco.setLogradouro(retornoValues[1].replaceAll("[,\"]",""));
                } else if(retornoValues[0].matches(("[ \t]+\"complemento\""))){
                    endereco.setComplemento(retornoValues[1].replaceAll("[,\"]", ""));
                } else if(retornoValues[0].matches("[ \t]+\"bairro\"")){
                    endereco.setBairro(retornoValues[1].replaceAll("[,\"]", ""));
                } else if(retornoValues[0].matches("[ \t]+\"localidade\"")){
                    endereco.setCidade(retornoValues[1].replaceAll("[,\"]", ""));
                } else if(retornoValues[0].matches("[ \t]+\"uf\"")){
                    endereco.setEstado(retornoValues[1].replaceAll("[ ,\"]", ""));
                }
            }
        } catch(IOException ex){
            System.out.println("IOexception busca cep online");
            System.out.println("Exception " + ex);
            
            
        }
        return endereco;
    }
    
    public static boolean[] categoriasFromString(String categorias){
        boolean[] b = new boolean[5];
        if(categorias.length() != 5){
            return null;
        } else {
            char letraAtual = 'A';
            for(int i = 0; i < 5; i++){
                b[i] = (categorias.charAt(i) == letraAtual);
                letraAtual++; // de 'A' a 'E';
            }
        
        }
        return b;
    }
    
    public static String categoriasToString(boolean[] categorias){
        String str = "";
        if(categorias.length == 5){
            char letraAtual = 'A';
            for(int i = 0; i < 5; i++){
                if(categorias[i]){
                    str +=  letraAtual;
                } else {
                    str += 'X';
                }
                letraAtual++;
            }
            return str;
        } else return null;
    }
    
    /**
     * Retorna se um nome é válido.
     * @return falso se o nome tiver três ou menos letras.
     */
    public static boolean isNameValid(String name){
        return name.replaceAll("[^A-Za-z]", "").length() > 3;
    }
    
    
    public static boolean isMaiorDeIdade(Date dtNascimento){
        Calendar now =  Calendar.getInstance();
        Calendar then = new Calendar.Builder().setInstant(dtNascimento.getTime()).build();
        int yearNow  = now.get(Calendar.YEAR);
        int yearThen = then.get(Calendar.YEAR);
        if(yearNow - yearThen > 18){
            return true;
        } else if(yearNow - yearThen == 18){
            return now.get(Calendar.DAY_OF_YEAR) > then.get(Calendar.DAY_OF_YEAR);
        } else {
            return false;
        }
    }   
    
    public static String dateToString(Date dt){
        return new SimpleDateFormat("dd/MM/yyyy").format(dt);
    }
    
    public static String semAcento(String s){
        return Normalizer.normalize(s, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }
}
