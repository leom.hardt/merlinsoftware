/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas;

import ifrs.scali.Main;
import ifrs.scali.database.FuncionarioDB;
import ifrs.scali.model.Funcionario;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Samsung
 */
public class FXMLLoginController implements Initializable {

    @FXML
    private TextField usernameField;
    @FXML
    private TextField passwordField;
    @FXML
    private Button entrarButton;
    @FXML
    private Hyperlink cadastrarButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void entrarAction() {
        Funcionario u = FuncionarioDB.autenticar(usernameField.getText(),passwordField.getText());
        if(u != null){
            Main.setUser(u); 
            Main.puxarNovamente();
            System.out.print("Tipo de usuário:");
            switch(u.getTipoUsuario()){
                case FUNCIONARIO:
                    System.out.println("Funcionário!");
                    break;
                case VENDEDOR:
                    System.out.println("Vendedor!");
                    Main.trocarDeTela("vendedor/FXMLTelaInicialVendedor.fxml");
                    break;
                case FINANCEIRO:
                    System.out.println("Financeiro!");
                    break;
                case ADMINISTRADOR:
                    System.out.println("Administrador!");
                    Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");
                    break;
                case MOTORISTA:
                    System.out.println("Motorista!");
                    Main.trocarDeTela("motorista/FXMLTelaInicialMotorista.fxml");
                    break;
            }
        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Login Falhou!", "Usuário ou Senha errados");
        }
    }

    @FXML
    private void cadastrarAction(ActionEvent event) {
        Main.trocarDeTela("FXMLCadastrar.fxml");
    }
    
}
