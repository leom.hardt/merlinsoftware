/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas.administrador;

import ifrs.scali.Main;
import ifrs.scali.database.FuncionarioDB;
import ifrs.scali.database.MotoristaDB;
import ifrs.scali.model.Funcionario;
import ifrs.scali.model.Motorista;
import ifrs.scali.model.TipoUsuario;
import ifrs.scali.model.Util;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author larac
 */
public class FXMLCadastrarFuncionarioController implements Initializable {

    @FXML
    private TextField pesquisarTF;
    @FXML
    private ChoiceBox<String> tipoCB;
    @FXML
    private Label nome;
    @FXML
    private Label funcao;
    @FXML
    private TextField nomeTF;
    @FXML
    private TextField documentoTF;
    
    @FXML
    private Button cadastrar;
    @FXML
    private TextField habilitacaoTF;
    @FXML
    private DatePicker nascimentoDP;
    @FXML
    private DatePicker dataHabilitacaoDP;
    @FXML
    private DatePicker vencimentoDP;
    @FXML
    private CheckBox catA;
    @FXML
    private CheckBox catB;
    @FXML
    private CheckBox catC;
    @FXML
    private CheckBox catD;
    @FXML
    private TextField usuarioTF;
    @FXML
    private TextField senhaTF;
    @FXML
    private CheckBox catE;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configurarLabelUsuario();
        tipoCB.setItems(
            FXCollections.observableList(
                Arrays.asList(
                    "Vendedor",
                    "Financeiro",
                    "Administrador",
                    "Motorista"
                )
        ));
        tipoCB.setValue("");
        
        tipoCB.getSelectionModel().selectedItemProperty().addListener((ChangeListener)((obs,oldv,newv) -> {
            boolean isMotorista = newv.equals("Motorista");
            catA.setDisable(! isMotorista);
            catB.setDisable(! isMotorista);
            catC.setDisable(! isMotorista);
            catD.setDisable(! isMotorista);
            catE.setDisable(! isMotorista);
            habilitacaoTF.setDisable(! isMotorista);
            vencimentoDP.setDisable(! isMotorista);
            dataHabilitacaoDP.setDisable(! isMotorista);
        }));
       
    }    
    
    private void configurarLabelUsuario(){
        nome.setText(Main.getUser().getUsername());
        funcao.setText(Main.getUser().getTipoUsuarioStr());
    }
        
    @FXML
    public void cadastrarMenuAction() {
        Main.trocarDeTela("administrador/FXMLEscolherCadastrar.fxml");
    }
    
    @FXML
    public void relatorioFinanceiroMenuAction() {
        Main.trocarDeTela("administrador/FXMLRelatorioFinanceiro.fxml");
    }
    
    @FXML
    public void telaInicialAction(){
        Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");

    }

    @FXML
    private void cadastrarAction(ActionEvent event) {
        Funcionario f = new Motorista();
        // Verificando o nome.
        if(nomeTF.getText().replaceAll("[^a-zA-z]+", "").length() >= 4){
            f.setNome(nomeTF.getText());
        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Erro!", "Nome curto demais.");
            return;
        }
        
        // Verificando o CPF.
        if(Util.isCpfValid(documentoTF.getText())){
           f.setCpf(Util.cpfToLong(documentoTF.getText()));
        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Erro!", "CPF Inválido.");
            return;
        }
        // Verificando o Nascimento.
        if(Util.isMaiorDeIdade(Date.valueOf(nascimentoDP.getValue()))){
            f.setDataNascimento(Date.valueOf(nascimentoDP.getValue()));
        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Erro!", "Motorista menor de idade.");
            return;
        }
        // Verificando usuário e senha
        if(!usuarioTF.getText().isEmpty()){
            f.setUsername(usuarioTF.getText());
        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Erro!", "Não é possível deixar o nome de usuário em branco.");
            return;
        }
        if(!senhaTF.getText().isEmpty()){
            f.setPassword(senhaTF.getText());
        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Erro!", "Não é possível deixar a senha em branco.");
            return;
        }
        // Verificando o tipo
        switch(tipoCB.getValue()){
            case "":
                Main.showPopup(Alert.AlertType.ERROR, "Erro!", "Tipo de usuário não definido.");
                return;
            case "Motorista":
                f.setTipoUsuario(TipoUsuario.MOTORISTA);
                Motorista m = new Motorista(f);
                m.setCategorias(
                    catA.isSelected(), catB.isSelected(), catC.isSelected(), 
                    catD.isSelected(), catE.isSelected()
                );
                
                System.out.println("M set categorias: " + catA.isSelected() + catB.isSelected() +catC.isSelected() +
                    catD.isSelected() + catE.isSelected());
                System.out.println("Categorias: " + Arrays.toString(m.getCategorias()));
                System.out.println("Valor final: " + m.getCategoriasStr());
                m.setDataHabilitacao(Date.valueOf(dataHabilitacaoDP.getValue()));
                m.setDataVencimento(Date.valueOf(vencimentoDP.getValue()));
                m.setNumHabilitacao(Integer.parseInt(habilitacaoTF.getText()));
                MotoristaDB.insert(m);
                return;
            case "Vendedor":
                f.setTipoUsuario(TipoUsuario.VENDEDOR);
                break;
            case "Administrador":
                f.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
                break;
            case "Financeiro":
                f.setTipoUsuario(TipoUsuario.FINANCEIRO);
                
                break;
        }
        Funcionario result = FuncionarioDB.insert(f);
        if(result != null){
//            Main.funcionarios.add(result);
            Main.showPopup(Alert.AlertType.INFORMATION, "Sucesso!", 
                    "Inserido com sucesso.");
            Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");
        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Erro!","Não conseguimos cadastrar. Possível duplicata de outro funcionário");
        }
    }
    
    

    @FXML
    private void cancelarAction(ActionEvent event) {
    }
    
}
