/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas.administrador;

import ifrs.scali.Main;
import ifrs.scali.database.ClienteDB;
import ifrs.scali.model.Cliente;
import ifrs.scali.model.Endereco;
import ifrs.scali.model.Util;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Ana/Léo
 */
public class FXMLGerenciarClienteController implements Initializable {
    
    @FXML
    private TextField nomeTF;

    @FXML
    private TextField telefoneTF;

    @FXML
    private TextField ruaTF;

    @FXML
    private TextField bairroTF;

    @FXML
    private TextField cidadeTF;

    @FXML
    private TextField documentoTF;

    @FXML
    private TextField cepTF;

    @FXML
    private TextField numeroTF;

    @FXML
    private TextField ufTF;

    @FXML
    private TextField complementoTF;

    @FXML
    private RadioButton juridicaTF;

    @FXML
    private RadioButton fisicaTF;
    
    private Cliente cliente;
    
    @FXML
    private Label funcao;
    
    @FXML
    private TextField pesquisarTF;
    
    
    @FXML
    private Label nome;
    @FXML
    private Button cadastrar;
    @FXML
    private ChoiceBox<?> tipoCB;

    private void buscarCep(){
        Endereco end = Util.buscarPorCepOnline(cepTF.getText());
        bairroTF.setText(end.getBairro());
        cidadeTF.setText(end.getCidade());
        complementoTF.setText(end.getComplemento());
        ruaTF.setText(end.getLogradouro());
        ufTF.setText(end.getEstado());
        
    }
    
    private void configurarLabelUsuario(){
        nome.setText(Main.getUser().getUsername());
        funcao.setText(Main.getUser().getTipoUsuarioStr());
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configurarLabelUsuario();
        inicializarCampos();
    }
    
    /**
     * Inicializa todos os campos de edição com os valores que o usuário solicitou.
     */
    private void inicializarCampos(){
        int idCliente = FXMLTelaInicialController.idSelecionado;
        cliente = ClienteDB.getOne(idCliente);
        if(cliente != null){
            nomeTF.setText(cliente.getName());
            documentoTF.setText(cliente.getDocumento());
            telefoneTF.setText(cliente.getTelefone());
            if(cliente.getEndereco() != null){
                cepTF.setText(cliente.getEndereco().getCep());
                buscarCep();
                numeroTF.setText(Integer.toString(cliente.getEndereco().getNumero()));
                complementoTF.setText(cliente.getEndereco().getComplemento());
            }
            
            switch(cliente.getTipo()){
                case PESSOA_JURIDICA:
                    juridicaTF.setSelected(true);
                    break;
                case PESSOA_FISICA:
                    fisicaTF.setSelected(true);
                    break;                
            }
            juridicaTF.setDisable(true);
            fisicaTF.setDisable(true);
            documentoTF.setDisable(true);
        } else {
            // Esse erro não pode acontecer em momento algum
            Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");
            Alert dialog = new Alert(Alert.AlertType.ERROR, "Cliente não encontrado!");
            dialog.showAndWait();
        }
    }
    
    
    @FXML
    public void salvarAction(){
        // Remove o cliente da lista do Main, para só depois adicioná-lo atualizado
        Main.clientes.removeIf(c -> (c.getId() == cliente.getId()));
        
        cliente.setName(nomeTF.getText());
        
        cliente.setTelefone(telefoneTF.getText());
        
        Endereco endereco = new Endereco();
        
        endereco.setEstado(ufTF.getText());// verificar valido
        endereco.setBairro(bairroTF.getText());
        endereco.setCep(cepTF.getText());
        endereco.setCidade(cidadeTF.getText());
        endereco.setLogradouro(ruaTF.getText());
        endereco.setNumero(Integer.parseInt(numeroTF.getText()));
        endereco.setComplemento(complementoTF.getText());
        
        cliente.setEndereco(endereco);
        System.out.println("cleinte id "+ cliente.getId());
        if (ClienteDB.update(cliente)){
            Main.showPopup(Alert.AlertType.INFORMATION, "Sucesso!", "Editado com sucesso!");
            Main.clientes.add(cliente);
        }
    }
    
    @FXML
    public void cancelarAction() {
        Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");
    }
    
    @FXML
    public void telaInicialAction() {
        Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");
    }

    @FXML
    public void relatorioFinanceiroMenuAction() {
        Main.trocarDeTela("administrador/FXMLRelatorioFinanceiro.fxml");
    }

    
        
    @FXML
    public void cadastrarMenuAction() {
        Main.trocarDeTela("administrador/FXMLEscolherCadastrar.fxml");
    }
    
    
    public void relatorioFinanceiroAction() {
        Main.trocarDeTela("administrador/FXMLRelatorioFinanceiro.fxml");
    }

    @FXML
    private void deletarAction(ActionEvent event) {
        boolean deleted = ClienteDB.virtualDelete(FXMLTelaInicialController.idSelecionado);
        if(deleted){
            // Remove só o cliente com aquele ID.
            Main.clientes.removeIf(c -> (c.getId() == cliente.getId()));
            Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");
        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Erro!","Não conseguimos deletar.");
        }
    }
    
}
