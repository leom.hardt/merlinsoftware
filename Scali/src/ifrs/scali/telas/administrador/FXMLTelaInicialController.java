/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas.administrador;

import ifrs.scali.Main;
import ifrs.scali.database.ClienteDB;
import ifrs.scali.database.ConsultasGenericasDB;
import ifrs.scali.database.MotoristaDB;
import ifrs.scali.database.VeiculoDB;
import ifrs.scali.model.Cliente;
import ifrs.scali.model.Motorista;
import ifrs.scali.model.Util;
import ifrs.scali.model.Veiculo;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 * 
 *
 * @author Ketlin
 */
public class FXMLTelaInicialController implements Initializable {
    
    
    
    @FXML
    private TableView tabela;
    @FXML
    private TableColumn<?, ?> idCol;
    @FXML
    private TableColumn<?, ?> clienteCol;
    @FXML
    private TableColumn<?, ?> partidaCol;
    @FXML
    private TableColumn<?, ?> destinoCol;
    @FXML
    private TableColumn<?, ?> placaCol;
    @FXML
    private TableColumn<?, ?> motoristaCol;
    @FXML
    private TableColumn<?, ?> veiculoCol;
    @FXML
    private TableColumn<?, ?> descricaoCol;
    @FXML
    private TableColumn<?, ?> valorCol;
    @FXML
    private TextField pesquisarTF;
    @FXML
    private ChoiceBox<String> tipoCB;
    @FXML
    private TableColumn<?, ?> enderecoCol;
    @FXML
    private TableColumn<?, ?> nomeCol;
    @FXML
    private TableColumn<?, ?> cpfCol;
    
    public static int idSelecionado;
    @FXML

    private AnchorPane pane;

    @FXML
    private Label funcao;
    @FXML
    private Label nome;
    @FXML
    private Label novosClientes;
    @FXML
    private Label pedidosFinalizados;
    @FXML
    private Label novosPedidos;
    @FXML
    private Label pedidosAndamento;
    @FXML
    private Label veiculosDisponiveis;
    @FXML
    private TableColumn<?, ?> categoriasCol;
    @FXML
    private TableColumn<?, ?> vencimentoCol;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
        // Quando pesquisar, reinicia o filtro.
        pesquisarTF.textProperty().addListener((obs, oldVal, newVal) -> {
            filtrar();            
        });
        
        // Inicializa a tabela como para mostrar clientes.
        tabela.setItems(FXCollections.observableArrayList(Main.clientes));
        tipoCB.setItems(FXCollections.observableArrayList("Cliente", "Motorista", "Veículo"));
        tipoCB.setValue("Cliente");
        configurarColunasCliente();
        
        // Quando mudar o tipo de pesquisa, também reincia o filtro.
        tipoCB.valueProperty().addListener((obs, oldVal, newVal) -> {
            tabela.getItems().clear();
            switch(tipoCB.getSelectionModel().getSelectedIndex()){
                case 0:
                    configurarColunasCliente();
                    break;
                case 1:
                    configurarColunasMotorista();
                    break;
                case 2:
                    configurarColunasVeiculo();
                    break;
            }
            
            filtrar();
        });
        configurarLabelUsuario();
    }
   
    private void configurarLabelUsuario(){
        nome.setText(Main.getUser().getUsername());
        funcao.setText(Main.getUser().getTipoUsuarioStr());
    }
    
    private void puxarDoBanco(){
        Main.puxarNovamente();
        // Carregar aqueles Labels do lado
        novosClientes.setText(Integer.toString(ConsultasGenericasDB.getNumberOf(
                "SELECT count(id) from mr_cliente where data_cadastro > (SELECT CURRENT_DATE FROM DUAL)")));
        pedidosFinalizados.setText(Integer.toString(ConsultasGenericasDB.getNumberOf(
                "select count(id) from mr_encomenda where status = 5")));
        novosPedidos.setText(Integer.toString(ConsultasGenericasDB.getNumberOf(
                "select count(id) from mr_encomenda where DATA_SOLICITACAO > (SELECT CURRENT_DATE FROM DUAL)")));
        pedidosAndamento.setText(Integer.toString(ConsultasGenericasDB.getNumberOf(
                "select count(id) from mr_encomenda where status = 4")));
        veiculosDisponiveis.setText(Integer.toString(ConsultasGenericasDB.getNumberOf(
                "select count(mr_veiculo.id) from mr_veiculo inner join mr_viagem on "
                        + "mr_veiculo.id = mr_viagem.veiculo inner join mr_encomenda on"
                        + " mr_viagem.id = MR_ENCOMENDA.viagem where MR_ENCOMENDA.status=5 and"
                        + " MR_ENCOMENDA.status != 4 and MR_ENCOMENDA.status != 3 and MR_ENCOMENDA.status != 2")));
        
    }
    
    
     public void configurarColunasVeiculo(){
        idCol.setCellValueFactory(new PropertyValueFactory("id"));
        descricaoCol.setCellValueFactory(new PropertyValueFactory("descricao"));
        placaCol.setCellValueFactory(new PropertyValueFactory("placa"));
        idCol.setVisible(true);
        placaCol.setVisible(true);
        descricaoCol.setVisible(true);
        //
        cpfCol.setVisible(false);
        nomeCol.setVisible(false);
        enderecoCol.setVisible(false);
        clienteCol.setVisible(false);
        partidaCol.setVisible(false);
        destinoCol.setVisible(false);
        motoristaCol.setVisible(false);
        valorCol.setVisible(false);
        veiculoCol.setVisible(false);
    }
    
    public void configurarColunasCliente(){
        idCol.setCellValueFactory(new PropertyValueFactory("id"));
        nomeCol.setCellValueFactory(new PropertyValueFactory("name"));
        nomeCol.setVisible(true);
        cpfCol.setCellValueFactory(new PropertyValueFactory("documento"));
        cpfCol.setVisible(true);
        enderecoCol.setCellValueFactory(new PropertyValueFactory("enderecoResumido"));
        enderecoCol.setVisible(true);
        //
        clienteCol.setVisible(false);
        partidaCol.setVisible(false);
        destinoCol.setVisible(false);
        motoristaCol.setVisible(false);
        valorCol.setVisible(false);
        veiculoCol.setVisible(false);
        categoriasCol.setVisible(false);
        vencimentoCol.setVisible(false);
        placaCol.setVisible(false);
        descricaoCol.setVisible(false);
        
    }
    
    
    public void configurarColunasMotorista(){
        idCol.setCellValueFactory(new PropertyValueFactory("id"));
        nomeCol.setCellValueFactory(new PropertyValueFactory("nome"));
        cpfCol.setCellValueFactory(new PropertyValueFactory("documento"));
        vencimentoCol.setCellValueFactory(new PropertyValueFactory("vencimentoStr"));
        categoriasCol.setCellValueFactory(new PropertyValueFactory("categoriasStr"));
        cpfCol.setVisible(true);
        categoriasCol.setVisible(true);
        vencimentoCol.setVisible(true);
        // Deixando invisíveis as outras colunas.
        enderecoCol.setVisible(false);
        clienteCol.setVisible(false);
        partidaCol.setVisible(false);
        destinoCol.setVisible(false);
        motoristaCol.setVisible(false);
        valorCol.setVisible(false);
        veiculoCol.setVisible(false);
        placaCol.setVisible(false);
        descricaoCol.setVisible(false);
    }
   
    private void filtrar(){
        switch(tipoCB.getSelectionModel().getSelectedIndex()){
            case 0:{ // "Cliente"
                
                ArrayList<Cliente> filtrado = new ArrayList<>(); 
                for(int i =0; i < Main.clientes.size(); i++){
                    Cliente u = Main.clientes.get(i);
                    String nomeSemAcento = Util.semAcento(u.getName());
                    if(nomeSemAcento.matches(".*(?i)" + pesquisarTF.getText() + ".*")
                            ||Util.cpfFromLong(u.getCpf()).matches(".*" +  pesquisarTF.getText() + ".*")
                            ||Util.cnpjFromLong(u.getCnpj()).matches(".*" +  pesquisarTF.getText() + ".*")
                    ){
                      filtrado.add(u);
                    }
                    
                }
                tabela.setItems(FXCollections.observableArrayList(filtrado)); 
                break;
            }
            case 1:{ // "Motorista"
                ArrayList<Motorista> filtrado = new ArrayList<>();
                for(int i =0; i < Main.motoristas.size(); i++){
                    Motorista u = Main.motoristas.get(i);
                    filtrado.add(u);
                }
                tabela.setItems(FXCollections.observableArrayList(filtrado)); 
                break;
            }
            case 2:{ // "Veículo"
                ArrayList<Veiculo> filtrado = new ArrayList<>();
                for(int i =0; i < Main.veiculos.size(); i++){
                    Veiculo u = Main.veiculos.get(i);
                    filtrado.add(u);
                }
                tabela.setItems(FXCollections.observableArrayList(filtrado)); 
                break;
            }
        }
    }
    
    @FXML
    private void selecionarAction() {
        Object object = tabela.getSelectionModel().getSelectedItem();
        if(object != null){
            String classe = object.getClass().getName();
            switch (classe){
                case "ifrs.scali.model.Cliente":
                    Cliente cliente = (Cliente) object;
                    idSelecionado = cliente.getId();
                    Main.trocarDeTela("administrador/FXMLGerenciarCliente.fxml");
                    break;
                case "ifrs.scali.model.Motorista":
                    Motorista motorista = (Motorista) object;
                    idSelecionado = motorista.getId();
                    Main.trocarDeTela("administrador/FXMLGerenciarFuncionario.fxml");
                    break;
                case "ifrs.scali.model.Veiculo":
                    Veiculo veiculo = (Veiculo) object;
                    idSelecionado = veiculo.getId();
                    Main.trocarDeTela("administrador/FXMLGerenciarVeiculos.fxml");
                    break;
            }
        }
        
    }
    @FXML
    public void cadastrarAction() {
        Main.trocarDeTela("administrador/FXMLEscolherCadastrar.fxml");
    }
    
    @FXML
    public void relatorioFinanceiroAction() {
        Main.trocarDeTela("administrador/FXMLRelatorioFinanceiro.fxml");
    }
    
    @FXML
    public void atualizarAction(){
        puxarDoBanco();
        filtrar();
    }
    
}
