/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas.administrador;

import ifrs.scali.Main;
import ifrs.scali.database.TipoVeiculoDB;
import ifrs.scali.model.TipoVeiculo;
import ifrs.scali.model.Veiculo;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class FXMLCadastrarVeiculosController implements Initializable {

    @FXML
    private TextField pesquisarTF;

    @FXML
    private ChoiceBox<?> tipoCB;

    @FXML
    private Label nome;

    @FXML
    private Label funcao;

    @FXML
    private ChoiceBox<TipoVeiculo> tipoVeiculoCB;

    @FXML
    private TextField placaTF;

    @FXML
    private TextField anoTF;

    @FXML
    private TextField renavamTF;

    @FXML
    private TextField chassiTF;

    @FXML
    private TextField taraTF;

    @FXML
    private TextField capacidadeM3TF;

    @FXML
    private TextField capacidadeKgTF;

    @FXML
    private ChoiceBox<?> combustivelCB;

    @FXML
    private TextField KmTF;

    @FXML
    private Button cadastrar;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tipoVeiculoCB.setItems(FXCollections.observableList(TipoVeiculoDB.getAllTipoVeiculos()));
        configurarLabelUsuario();
    }    
    
    private void configurarLabelUsuario(){
        nome.setText(Main.getUser().getUsername());
        funcao.setText(Main.getUser().getTipoUsuarioStr());
    }
    
        
    @FXML
    public void cadastrarMenuAction() {
        Main.trocarDeTela("administrador/FXMLEscolherCadastrar.fxml");
    }
    
    @FXML
    public void relatorioFinanceiroMenuAction() {
        Main.trocarDeTela("administrador/FXMLRelatorioFinanceiro.fxml");
    }
    
    @FXML
    public void telaInicialAction(){
        Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");

    }

    @FXML
    private void cadastrarAction(ActionEvent event) {
        Veiculo veiculo = new Veiculo();
        veiculo.setPlaca(placaTF.getText());
        veiculo.setAnoFabricacao(Integer.parseInt(anoTF.getText()));
        veiculo.setRenavam(Long.parseLong(renavamTF.getText()));
        veiculo.setNumeroChassi(chassiTF.getText());
        veiculo.setTaraKg(Double.parseDouble(taraTF.getText()));
        veiculo.setCapacidadeKg(Double.parseDouble(capacidadeKgTF.getText()));
        veiculo.setCapacidadeM3(Double.parseDouble(capacidadeM3TF.getText()));
        veiculo.setKmAtual(Integer.parseInt(KmTF.getText()));        
    }

    @FXML
    private void cancelarAction(ActionEvent event) {
    }
    
}
