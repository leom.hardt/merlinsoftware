/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas.administrador;

import ifrs.scali.Main;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class FXMLGerenciarVeiculosController implements Initializable {

    @FXML
    private TextField pesquisarTF;
    @FXML
    private ChoiceBox<?> tipoCB;
    @FXML
    private Label nome;
    @FXML
    private Label funcao;
    @FXML
    private TextField ruaTF;
    @FXML
    private TextField cepTF;
    @FXML
    private Button cadastrar;
    @FXML
    private TextField documentoTF1;
    @FXML
    private TextField telefoneTF1;
    @FXML
    private TextField telefoneTF11;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configurarLabelUsuario();
    }    
    
    private void configurarLabelUsuario(){
        nome.setText(Main.getUser().getUsername());
        funcao.setText(Main.getUser().getTipoUsuarioStr());
    }
    
    @FXML
    public void cadastrarMenuAction() {
        Main.trocarDeTela("administrador/FXMLEscolherCadastrar.fxml");
    }
    
    @FXML
    public void relatorioFinanceiroMenuAction() {
        Main.trocarDeTela("administrador/FXMLRelatorioFinanceiro.fxml");
    }
    
    @FXML
    public void telaInicialAction(){
        Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");

    }

    @FXML
    private void cadastrarAction(ActionEvent event) {
    }

    @FXML
    private void cancelarAction(ActionEvent event) {
    }
    
}
