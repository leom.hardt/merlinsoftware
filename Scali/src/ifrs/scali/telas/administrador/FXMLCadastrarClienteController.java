/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas.administrador;

import ifrs.scali.Main;
import ifrs.scali.database.ClienteDB;
import ifrs.scali.model.Cliente;
import ifrs.scali.model.Cliente.TipoCliente;
import ifrs.scali.model.Endereco;
import ifrs.scali.model.Util;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class FXMLCadastrarClienteController implements Initializable {

    @FXML
    private Label nome;
    @FXML
    private Label funcao;//se é motorista ou adm
    @FXML
    private Button cadastrar;
    @FXML
    private TextField telefoneTF;
    @FXML
    private TextField ruaTF;
    @FXML
    private TextField bairroTF;
    @FXML
    private TextField cidadeTF;
    @FXML
    private TextField cepTF;
    @FXML
    private TextField documentoTF;
    @FXML
    private TextField numeroTF;
    @FXML
    private TextField ufTF;
    @FXML
    private TextField complementoTF;
    @FXML
    private RadioButton fisicaTF;
    @FXML
    private RadioButton juridicaTF;
    @FXML
    private TextField pesquisarTF;
    @FXML
    private ChoiceBox<String> tipoCB;
    @FXML
    private TextField nomeTF;
    
           
          
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tipoCB.setItems(FXCollections.observableArrayList("Cliente", "Motorista", "Veículo"));
        cepTF.textProperty().addListener((observable) -> {
            if(cepTF.getText().length() == 8 && !cepTF.getText().contains("-")){
                    cepTF.setText(Util.cepFromLong(Long.parseLong(cepTF.getText())));
            }
            if(Util.isCepValid(cepTF.getText())){
                buscarCep();
            }
        });
        
        configurarLabelUsuario();
    }    
    
    private void configurarLabelUsuario(){
        nome.setText(Main.getUser().getUsername());
        funcao.setText(Main.getUser().getTipoUsuarioStr());
    }
    
    private void buscarCep(){
        Endereco end = Util.buscarPorCepOnline(cepTF.getText());
        bairroTF.setText(end.getBairro());
        cidadeTF.setText(end.getCidade());
        complementoTF.setText(end.getComplemento());
        ruaTF.setText(end.getLogradouro());
        ufTF.setText(end.getEstado());
    }
    
    
    @FXML
    public void cadastrarMenuAction() {
        Main.trocarDeTela("administrador/FXMLEscolherCadastrar.fxml");
    }
    
    @FXML
    public void relatorioFinanceiroMenuAction() {
        Main.trocarDeTela("administrador/FXMLRelatorioFinanceiro.fxml");
    }
    
    @FXML
    public void telaInicialAction(){
        Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");

    }
    
    @FXML 
    public void cadastrarAction(){
        Cliente c = new Cliente();
        Date dtCadastro = new Date(System.currentTimeMillis());
        c.setCadastro(dtCadastro);
        
        if(Util.isNameValid(nomeTF.getText())){
            c.setName(nomeTF.getText());
        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Nome Inválido", 
                    "Nome deve possuir mais de 3 caracteres alfanuméricos.");
            return;
        }
        
        if(fisicaTF.isSelected()){
            c.setTipo(TipoCliente.PESSOA_FISICA);
            c.setCpf(Util.cpfToLong(documentoTF.getText()));
            if(c.getCpf() == -1){
                Main.showPopup(Alert.AlertType.ERROR, "CPF Inválido", 
                        "O CPF não é válido.");
                return;
            }
        } else if(juridicaTF.isSelected()){
            c.setTipo(TipoCliente.PESSOA_JURIDICA);
            c.setCnpj(Util.cnpjToLong(documentoTF.getText()));
            if(c.getCnpj() == -1){
                Main.showPopup(Alert.AlertType.ERROR, "CNPJ Inválido", 
                        "O CNPJ é inválido.");
                return;
            }

        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Erro!", 
                    "Tipo de Cliente não foi selecionado.");
            return;
        }
        
        c.setTelefone(telefoneTF.getText());
        
        Endereco endereco = new Endereco();
        
        endereco.setEstado(ufTF.getText());
        if(! Util.isEstadoValid(endereco.getEstado())){
            
            Main.showPopup(Alert.AlertType.ERROR, "Estado Inválido!", 
                    "Estado deve ser uma sigla maiúscula. Ex: RS.");
        }
        
        endereco.setBairro(bairroTF.getText());
        endereco.setCep(cepTF.getText());
        endereco.setCidade(cidadeTF.getText());
        endereco.setLogradouro(ruaTF.getText());
        endereco.setNumero(Integer.parseInt(numeroTF.getText()));
        endereco.setComplemento(complementoTF.getText());
        
        c.setEndereco(endereco);
        Cliente result = ClienteDB.insert(c);
        if(result != null){
            Main.clientes.add(result);
            Main.showPopup(Alert.AlertType.INFORMATION, "Sucesso!", 
                    "Inserido com sucesso. Data do cadastro: " 
                            + Util.dateToString(dtCadastro));
            Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");
        } else {
            Main.showPopup(Alert.AlertType.ERROR, "Erro!", 
                    "Não foi inserido no banco de dados. Possível duplicata "
                            + "de outro já inserido.");
        }
    }

    @FXML
    private void cancelarAction(ActionEvent event) {
        Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");
    }
}
