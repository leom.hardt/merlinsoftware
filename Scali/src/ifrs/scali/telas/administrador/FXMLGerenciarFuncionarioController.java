/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas.administrador;

import ifrs.scali.Main;
import ifrs.scali.database.MotoristaDB;
import ifrs.scali.model.Motorista;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class FXMLGerenciarFuncionarioController implements Initializable {

    @FXML
    private TextField pesquisarTF;
    @FXML
    private ChoiceBox<?> tipoCB;
    @FXML
    private Label nome;
    @FXML
    private Label funcao;
    @FXML
    private TextField nomeTF;
    @FXML
    private TextField telefoneTF;
    @FXML
    private TextField documentoTF;
    @FXML
    private Button cadastrar;
    @FXML
    private CheckBox catA;
    @FXML
    private CheckBox catB;
    @FXML
    private CheckBox catC;
    @FXML
    private CheckBox catD;
    @FXML
    private CheckBox catE;
    @FXML
    private DatePicker dtHabilitacaoDP;
    @FXML
    private DatePicker nascimentoDP;
    @FXML
    private TextField ctpsTF;
    @FXML
    private DatePicker vencDP;
    @FXML
    private TextField usuarioTF;
    @FXML
    private TextField senhaTF;
    @FXML
    private ChoiceBox<?> statusCB;

   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configurarLabelUsuario();
        inicializarCampos();
        
    }    
    
    private void configurarLabelUsuario(){
        nome.setText(Main.getUser().getUsername());
        funcao.setText(Main.getUser().getTipoUsuarioStr());
    }
    
    private void inicializarCampos(){
        Motorista m = MotoristaDB.getOne(FXMLTelaInicialController.idSelecionado);
        if(m != null){
            nomeTF.setText(m.getNome());
            documentoTF.setText(m.getDocumento());
            boolean cat[] = m.getCategorias();
            catA.setSelected(cat[0]);
            catB.setSelected(cat[1]);
            catC.setSelected(cat[2]);
            catD.setSelected(cat[3]);
            catE.setSelected(cat[4]);
            telefoneTF.setDisable(true);
            dtHabilitacaoDP.setValue(m.getDataHabilitacao().toLocalDate());
            nascimentoDP.setValue(m.getDataNascimento().toLocalDate());
            vencDP.setValue(m.getDataVencimento().toLocalDate());
            // usuario e senha?
            
        } else {
            // Esse erro não pode acontecer em momento algum
            Main.trocarDeTela("FXMLTelaInicial.fxml");
            Alert dialog = new Alert(Alert.AlertType.ERROR, "Funcionário não encontrado!");
            dialog.showAndWait();
        }
    }
    
    @FXML
    public void cadastrarMenuAction() {
        Main.trocarDeTela("administrador/FXMLEscolherCadastrar.fxml");
    }
    
    @FXML
    public void relatorioFinanceiroMenuAction() {
        Main.trocarDeTela("administrador/FXMLRelatorioFinanceiro.fxml");
    }
    
    @FXML
    public void telaInicialAction(){
        Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");

    }

    @FXML
    private void cadastrarAction(ActionEvent event) {
    }

    @FXML
    private void cancelarAction(ActionEvent event) {
        Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");
    }
    
}
