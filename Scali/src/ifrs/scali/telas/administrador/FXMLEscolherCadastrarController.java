/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas.administrador;

import ifrs.scali.Main;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author larac
 */
public class FXMLEscolherCadastrarController implements Initializable {
    @FXML
    private ChoiceBox<String> tipoCB;
    @FXML
    private Label funcao;
    @FXML
    private TextField pesquisarTF;
    @FXML
    private Label nome;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tipoCB.setItems(FXCollections.observableArrayList("Cliente", "Motorista", "Veículo"));
        
        configurarLabelUsuario();
                
    }    
    
    private void configurarLabelUsuario(){
        nome.setText(Main.getUser().getUsername());
        funcao.setText(Main.getUser().getTipoUsuarioStr());
    }
    
    @FXML
    public void cadastrarAction() {
        Main.trocarDeTela("administrador/FXMLEscolherCadastrar.fxml");
    }
    
    
    @FXML
    public void relatorioFinanceiroAction() {
        Main.trocarDeTela("administrador/FXMLRelatorioFinanceiro.fxml");
    }
    
    @FXML
    public void cadastrarClienteAction(){
        Main.trocarDeTela("administrador/FXMLCadastrarCliente.fxml");
    }
    
    @FXML
    public void cadastrarVeiculoAction(){
        Main.trocarDeTela("administrador/FXMLCadastrarVeiculos.fxml");
        
    }
    
    @FXML
    public void cadastrarFuncionarioAction(){
        Main.trocarDeTela("administrador/FXMLCadastrarFuncionario.fxml");
    }
    
    @FXML
    public void telaInicialAction(){
        Main.trocarDeTela("administrador/FXMLTelaInicial.fxml");
    }
}
