/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas.vendedor;

import ifrs.scali.database.ClienteDB;
import ifrs.scali.model.Cliente;
import ifrs.scali.model.Cobranca;
import ifrs.scali.model.Encomenda;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class FXMLGerarFreteController implements Initializable {

    @FXML
    private TextField pesquisarTF;

    @FXML
    private ChoiceBox<?> tipoCB;
    
    @FXML
    private Label nome;
    @FXML
    private Label funcao;
    @FXML
    private ChoiceBox<?> remetenteCB;
    @FXML
    private ChoiceBox<?> destinatarioCB;
    @FXML
    private DatePicker entregaDP;
    @FXML
    private DatePicker coletaDP;
    @FXML
    private CheckBox eCotacaoCB;
    @FXML
    private TextField diaCobrancaTF;
    @FXML
    private TextField valorTF;
    @FXML
    private TextField parcelasTF;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
        tipoCB.setItems(FXCollections.observableArrayList("Cliente", "Motorista", "Encomenda"));
        remetenteCB.setItems(FXCollections.observableList(ClienteDB.getAll()));
        destinatarioCB.setItems(FXCollections.observableList(ClienteDB.getAll()));
        
    }
        
    
    @FXML
    void cadastrarAction(ActionEvent event) {
        Cliente remetente =  (Cliente) remetenteCB.getSelectionModel().getSelectedItem();
        Cliente destinatario =  (Cliente) destinatarioCB.getSelectionModel().getSelectedItem();
        
        Encomenda encomenda = new Encomenda();
        Cobranca cobranca = new Cobranca();
        encomenda.setCobranca(cobranca);
        encomenda.setRemetente(remetente);
        encomenda.setDestinatario(destinatario);
        encomenda.setDtColeta(Date.valueOf(coletaDP.getValue()));
        encomenda.setDtEntrega(Date.valueOf(entregaDP.getValue()));
        
        /*switch (cliente){
          cobranca.setCliente(remetente);  
        }*/
        
        //cobranca.setIsCotacao(true);
        
    }

    @FXML
    void cancelarAction(ActionEvent event) {

    }

    @FXML
    void gerarAction(ActionEvent event) {

    }

    @FXML
    void recibosAction(ActionEvent event) {

    }

    @FXML
    void telaInicialAction(ActionEvent event) {

    }

    @FXML
    private void cadastrarAction(ActionEvent event) {
    }

    
}
