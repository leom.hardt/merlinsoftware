/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali.telas.motorista;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author larac
 */
public class FXMLTelaInicialMotoristaController implements Initializable {

    @FXML
    private Label funcao;
    @FXML
    private TextField pesquisarTF;
    @FXML
    private ChoiceBox<?> tipoCB;
    @FXML
    private Label nome;
    @FXML
    private TableView<?> tabela;
    @FXML
    private TableColumn<?, ?> idCol;
    @FXML
    private TableColumn<?, ?> clienteCol;
    @FXML
    private TableColumn<?, ?> partidaCol;
    @FXML
    private TableColumn<?, ?> destinoCol;
    @FXML
    private TableColumn<?, ?> motoristaCol;
    @FXML
    private TableColumn<?, ?> veiculoCol;
    @FXML
    private TableColumn<?, ?> valorCol;
    @FXML
    private TableColumn<?, ?> nomeCol;
    @FXML
    private TableColumn<?, ?> cpfCol;
    @FXML
    private TableColumn<?, ?> enderecoCol;
    @FXML
    private TableColumn<?, ?> categoriasCol;
    @FXML
    private TableColumn<?, ?> vencimentoCol;
    @FXML
    private TableColumn<?, ?> placaCol;
    @FXML
    private TableColumn<?, ?> descricaoCol;
    @FXML
    private Label novosClientes;
    @FXML
    private Label pedidosFinalizados;
    @FXML
    private Label novosPedidos;
    @FXML
    private Label pedidosAndamento;
    @FXML
    private Label veiculosDisponiveis;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void selecionarAction(MouseEvent event) {
    }

    @FXML
    private void telaInicialAction(ActionEvent event) {
    }

    @FXML
    private void relatorioFinanceiroMenuAction(ActionEvent event) {
    }
    
}
