/*
 * Copyright (C) 2019 Merlin Software
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ifrs.scali;

import ifrs.scali.database.ClienteDB;
import ifrs.scali.database.MotoristaDB;
import ifrs.scali.database.VeiculoDB;
import ifrs.scali.model.Cliente;
import ifrs.scali.model.Funcionario;
import ifrs.scali.model.Motorista;
import ifrs.scali.model.Util;
import ifrs.scali.model.Veiculo;
import java.io.IOException;
import java.util.List;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;

/**
 *
 * @author Léo H.
 */
public class Main extends Application {
    
    public static List<Cliente> clientes;
    public static List<Motorista> motoristas;
    public static List<Veiculo> veiculos;
    
    /** A tela na qual o usuário está utilizando o programa. */
    private static Stage root; 
    /** O usuário que está utilizando o programa. */
    private static Funcionario user;
    
    public static void main(String[] args) {
        launch(args);
        System.out.println(Util.cpfFromLong(66226514000L));
    }
    
    @Override
    public void start(Stage root){
        root.getIcons().add(new Image("imagens/icon.png"));
        root.setIconified(true);
        root.setTitle("Scali");
        this.root = root;
        trocarDeTela("FXMLLogin.fxml");
    }
    
    public static void trocarDeTela(String url){
        Node n = loadFromFile("telas/" + url);
        if(n != null){
            root.setScene(new Scene((Parent)n));
            root.show();
        } else {
            Main.showPopup(AlertType.ERROR, "Erro!","Não foi possível trocar de tela. \n\nURL: " + url );
            
        }
    }
    
    public static Node loadFromFile(String url) {
        try { 
            Node n = FXMLLoader.load(Main.class.getResource(url));
            return n;
        } catch(IOException | NullPointerException ex){
            
            Main.showPopup(AlertType.ERROR, "Erro!","URL de Arquivo inválida. \n\nErro: " + ex);
            ex.printStackTrace();
            return null;
        }
    }
    
    /**
     * O código a seguir cria um Alert e o mostra para o usuário.
     * Além disso, o configura para mostrar e esperar que o usuário pressione OK.
     * 
     * @param tipoErro O tipo de erro que será mostrado.
     * @param titulo   O título do Alert.
     * @param mensagem A mensagem que será mostrada no erro.
     */
    public static void showPopup(Alert.AlertType tipoErro, String titulo, String mensagem){
        Alert alert = new Alert(tipoErro);
        alert.setTitle(titulo);
        Label labelErro = new Label(mensagem);
        labelErro.setWrapText(true);
        alert.getDialogPane().setContent(labelErro);
        alert.showAndWait();
    }

    public static Funcionario getUser() {
        return user;
    }

    public static void setUser(Funcionario user) {
        Main.user = user;
    }
    
    public static void puxarNovamente(){
        // Inicialização dos Itens do Banco de Dados.
        System.out.println("Fetching Clientes");
        Main.clientes = ClienteDB.getAll();
        System.out.println("Fetching Motoristas");
        Main.motoristas = MotoristaDB.getAll();
        System.out.println("Fetching Veículos");
        Main.veiculos = VeiculoDB.getAllVeiculos();
        System.out.println("End Fetching.");
    }
    
}
